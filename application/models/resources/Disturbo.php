<?php

class Application_Resource_Disturbo extends Zend_Db_Table_Abstract {
	protected $_name    = 'disturbo';
    protected $_primary  = 'iddisturbo';
   	//protected $_rowClass = 'Application_Resource_Disturbo_Item';
	
	public function init(){}
	
	public function insertDisturbo($pippo){
		$this->insert($pippo);
	}
	public function getDisturbo()
    {
       $select= $this->select();
	   return $this->fetchAll($select);
    }
	public function cancellaDisturbo($iddisturbo){
		$where = "iddisturbo=".$iddisturbo;
		$this->delete($where);
	}
	public function getdisturbobyid($var)
    {
       return $this->find($var)->current();
    }
	
	public function updateDisturbo($data,$var) {
        $table = 'disturbo'; //nome tabella
        $where = 'iddisturbo='.$var; 
        $this->update($data, $where, $table);
    }
	public function recuperaDisturbi($id)
	{
		$select = $this->select()
                   		 ->where('iddisturbo IN(?)',$id);
        return $this->fetchAll($select);	
	}
	
}