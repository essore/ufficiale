<?php

class Application_Resource_Terapia extends Zend_Db_Table_Abstract {
	protected $_name = 'terapia';
	protected $_primary = 'idterapia';

	public function init() {
		
	}

	public function getTerapia($var){
		$ris = $this->select()->where('idpaziente='.$var)->order('data DESC');
		return $this->fetchAll($ris);
	}

	
	public function setNotifica($id){
		$var = array('notifica' => '0');
		$table = 'terapia'; 
        $where = 'idpaziente='.$id;
        $this->update($var, $where, $table);
	}
	
	public function addTerapia($var){
		$this->insert($var);
	}
	
	public function getTerapiaId($var){
		$where = 'clinico= \''.$var.'\'';
		$select = $this->select()
		->where($where)
		->order('data DESC')
		->limit(1);
		return $this->fetchRow($select);
	}

	public function filtrapaziente($datapre, $datapost, $var){
     $select= $this->select()->where('idpaziente = '.$var)
	 						 ->where('data > \''.$datapre.'\'')
	 						 ->where('data < \''.$datapost.'\'');
	  return $this->fetchAll($select);

			
		}
	

}
