<?php

class Application_Resource_Segue extends Zend_Db_Table_Abstract {
	protected $_name = 'segue';
	protected $_primary = array('idpazi','idclini');
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	public function newsegue($info) {
		$this->insert($info);
	}
	
	public function get($id) {
		return $this->find($id)->current();	
	}
	public function takeall(){
		$select= $this->select();
		return $this->fetchAll($select);
	}
	public function recuperasegue($id){
        $select = $this->select()
                        ->where('idpazi IN(?)', $id);
        return $this->fetchAll($select);	
	}	
	public function mypaziente($id){
		$where = 'idclini='.$id;
		$select = $this-> select()
		->where($where);
		return $this->fetchAll($select); 
	}
	public function notmypaziente($idp){
		$select = $this-> select()
		->where('idpazi not IN(?)', $idp)
		->group ('idpazi');
		return $this->fetchAll($select); 
	}
	
	public function seguitoDa($var){
		$ris =$this->select()
				   ->where('idpazi ='.$var);
		return $this->fetchAll($ris);
	}
	public function eliminasegue($where){
		$this->delete($where);
	}
	public function esistesegue($idc,$idp)
	{
		$ris =$this->select()
				   ->where('idclini = '.$idc)
				   ->where('idpazi = '.$idp);
		
		if(count($ris) != 0)return 1;
		else {
			return 0;
		} 
		
	}
	public function esistesegueb($idc,$idp)
	{
		$ris =$this->select()
				   ->where('idclini = '.$idc)
				   ->where('idpazi = '.$idp);
		return $this->fetchAll($ris);
		
	}
}
