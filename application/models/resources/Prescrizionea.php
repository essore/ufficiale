<?php

class Application_Resource_Prescrizionea extends Zend_Db_Table_Abstract {
	protected $_name = 'prescrizionea';
	protected $_primary = 'idter';

	public function getAttivita($var){
		return $this -> find($var) -> current();
	}
	
	public function prescriviAttivita($info){
		$this->insert($info);
	}
	
}
