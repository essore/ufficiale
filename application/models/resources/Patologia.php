<?php

class Application_Resource_Patologia extends Zend_Db_Table_Abstract {
	protected $_name = 'patologia';
	protected $_primary = array('idpaziente','iddisturbo');

	public function getDisturbiDi($var){
		$where = 'idpaziente='.$var;
		$sel= $this->select()
			 ->where($where);
		return $this->fetchAll($sel);
	}
	
	public function filtradisturbi($dist,$idp)
	{
		$select = $this->select()
                   		 ->where('iddisturbo = '.$dist.' AND idpaziente IN(?)',$idp);
        return $this->fetchAll($select);
	}
	public function esistePatologia($dist,$idp)
	{
		$select = $this->select()
                   		 ->where('iddisturbo = '.$dist.' AND idpaziente ='.$idp);
		$contatore=$this->fetchAll($select);
        if(count($contatore) > 0)return 1;
		else {
			return 0;
		} 
	}
	
	public function setpatologia($info)
	{
		return $this->insert($info);	
	}
	public function cancellaPatologia($where)//stringa del where sql
	{
		return $this->delete($where);
	}
}