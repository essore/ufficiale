<?php

class Application_Resource_Clinico extends Zend_Db_Table_Abstract {
	protected $_name = 'clinico';
	protected $_primary = 'idclinico';
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	public function getClinico() {
		$select = $this -> select();
		return $this -> fetchAll($select);
	}
	public function getClinicoById($id) {
		return $this->find($id)->current();
	}
	
	
	public function recuperaclinici($id) {
        $select = $this->select()
                        ->where('idclinico IN(?)', $id);
        return $this->fetchAll($select);	
	}

	public function getNrClinici(){
		$select = $this -> select();
		return $this -> fetchAll($select);
		
	}
	public function cancellaClinico($riga){
		$where = "idclinico=".$riga;
		$this->delete($where);
	}

	
	public function updateClinicoDati($data, $id) {
        $table = 'clinico'; //nome tabella
        $where = 'idclinico='.$id; 
        $this->update($data, $where, $table);
    }
	public function updateClinico($data, $id) {
        $table = 'clinico'; //nome tabella
        $where = 'idclinico='.$id; 
        $this->update($data, $where, $table);
    }

	public function newclinico($info){
		$this->insert($info);
	}
	public function recuperadirettori($id){
		$select = $this->select()
                        ->where('idclinico='.$id);
        return $this->fetchAll($select);
		
	}
	public function  getclinicibyRuolo($ruolo)
	{
		$select = $this->select()
                        ->where('ruolo = \''.$ruolo.'\'');
        return $this->fetchAll($select);
	}

}
