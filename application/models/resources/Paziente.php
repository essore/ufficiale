<?php

class Application_Resource_Paziente extends Zend_Db_Table_Abstract {
	protected $_name    = 'paziente';
    protected $_primary  = 'idpaziente';
  //  protected $_rowClass = 'Application_Resource_Utente_Item';
    
	public function init()
    {
    }
	
	// Estrae i dati della categoria $idutente
    public function getPaziente($id)
    {
	   return $this -> find($id) -> current();
    }
	public function getpazienti() {
		$select = $this->select();
       return $this->fetchAll($select);
	}
	public function newpaziente($info){
		$this->insert($info);
	}
	public function recuperapazienti($id){
        $select = $this->select()
                        ->where('idpaziente IN(?)', $id);
        return $this->fetchAll($select);	
	}
	public function filtrapazienti($where)
	{
		$select = $this->select()
                        ->where($where);
        return $this->fetchAll($select);
	}

	public function getNrPazienti ()
	{
		$select = $this->select();
       return $this->fetchAll($select);
	}

	
	public function updatePaziente($var, $id) {
        $table = 'paziente'; 
        $where = 'idpaziente='.$id; 
        $this->update($var, $where, $table);
    }


	public function notmypaziente($idp){
		$select = $this-> select()
		->where('idpaziente not IN(?)', $idp);
		return $this->fetchAll($select); }

	public function cancellapaziente($idpaziente){
		$where = "idpaziente=".$idpaziente;
		$this->delete($where);
	}
	public function savepaziente($pippo){
		$this->insert($pippo);

	}
	
	public function cercapazienti($idpazienti, $nome, $wild)
	{
		if($wild){
		$select = $this->select('idpaziente')
               	->where('idpaziente IN(?)', $idpazienti)
		 		->where('CONCAT(nome," ",cognome) LIKE ? OR CONCAT(cognome," ",nome) LIKE ?', $nome.'%' , $nome.'%');
		}else{
		$select = $this->select('idpaziente')
               	->where('idpaziente IN(?)', $idpazienti)
				->where('CONCAT(nome," ",cognome) LIKE ? OR CONCAT(cognome," ",nome) LIKE ?', $nome , $nome);
		}		
        return $this->fetchAll($select);	
	}

}
