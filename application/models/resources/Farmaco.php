<?php

class Application_Resource_Farmaco extends Zend_Db_Table_Abstract {
	protected $_name = 'farmaco';
	protected $_primary = 'idfarmaco';
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	public function getFarmacoById($var){
	
	   return $this -> find($var) -> current();
	}
	
	public function getAllFarmaco(){
		$ris = $this->select();
		return $this->fetchAll($ris);
	}
	
	public function getFarmaci(){
		$select = $this->select();
       return $this->fetchAll($select);
	   
	}

	public function insertFarmaco($values){
		$this->insert($values);
	}
	public function cancellaFarmaco($idfarmaco){
		$where = "idfarmaco=".$idfarmaco;
		$this->delete($where);
	}
	public function updateFarmaco($data,$var) {
        $table = 'farmaco'; //nome tabella
        $where = 'idfarmaco='.$var; 
        $this->update($data, $where, $table);
    }

}

