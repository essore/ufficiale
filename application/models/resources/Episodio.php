<?php

class Application_Resource_Episodio extends Zend_Db_Table_Abstract {
	protected $_name    = 'episodio';
    protected $_primary  = 'idepisodio';
   	//protected $_rowClass = 'Application_Resource_Disturbo_Item';
	
	public function init(){}
	
	public function insertEpisodio($pippo){
		$this->insert($pippo);
	}
	public function getEpisodio($var)
    {
       $select= $this->select()->where('paziente ='.$var)
	   							->order('data ASC');
	   return $this->fetchAll($select);
    }
	public function cancellaEpisodio($idepisodio){
		$where = "idepisodio=".$idepisodio;
		$this->delete($where);
	}

	public function getNrEventi(){
		$select=$this->select();
		return $this->fetchAll($select);
	}
	
	public function getNrEventiFiltroq1($datapre, $datapost){
		$select=$this->select()->where('data > \''.$datapre.'\'')
	 						 	->where('data < \''.$datapost.'\'');
		
		return $this->fetchAll($select);
	}
	public function getNrEventiFiltroq2($datapre, $datapost, $intensita){
		$select=$this->select()->where('data > \''.$datapre.'\'')
	 							->where('data < \''.$datapost.'\'')
								->where('intensita= \''.$intensita.'\'');
				
		return $this->fetchAll($select);
	}
	public function getNrEventiFiltroq3($datapre, $datapost, $disturbo){
		$select=$this->select()->where('data > \''.$datapre.'\'')
	 							->where('data < \''.$datapost.'\'')
								->where('disturbo='.$disturbo);
				
		return $this->fetchAll($select);
	}
	public function getNrEventiFiltroq4($datapre, $datapost, $intensita, $disturbo){
		$select=$this->select()->where('data > \''.$datapre.'\'')
	 							->where('data < \''.$datapost.'\'')
								->where('intensita= \''.$intensita.'\'')
								->where('disturbo='.$disturbo);
				
		return $this->fetchAll($select);
	}

	public function cancellatuttiEpisodi($id){
		$where = "paziente=".$id;
		$this->delete($where);
	}
	public function cancellaepisodifiltrati($id, $datapre, $datapost){
		$where = 'paziente= '.$id.' AND data > \''.$datapre.'\'' AND data < '\''.$datapost.'\'';
		$this->delete($where);
	}
	
	
	public function updateEpisodio($data, $idep){
		$table = 'episodio'; 
        $where = 'idepisodio= '.$idep; 
        $this->update($data, $where, $table);
	}

	public function filtrapaziente($datapre, $datapost, $var){
	     $select= $this->select()->where('paziente = '.$var)
	 						 ->where('data > \''.$datapre.'\'')
	 						 ->where('data < \''.$datapost.'\'')
							 ->order('data ASC');
		 return $this->fetchAll($select);
 		}
	public function filtrapa($var, $where){
	  $select= $this->select()->where($where)
	  						  ->where('paziente = '.$var);
	  		 return $this->fetchAll($select);
}
}