<?php

class Application_Resource_Attivita extends Zend_Db_Table_Abstract {
	protected $_name = 'attivita';
	protected $_primary = 'idattivita';
	//  protected $_rowClass = 'Application_Resource_Clinico_Item';

	public function init() {
	}

	public function getAttivitaById($var){
	
	   return $this -> find($var) -> current();

	}
	
	public function getAllAttivita(){
		$ris = $this->select();
		return $this->fetchAll($ris);

	} 
	public function getAttivita(){
	$select = $this->select();
       return $this->fetchAll($select);
	}
	public function insertAttivita($values){
		$this->insert($values);
	}
	public function cancellaAttivita($idattivita){
		$where = "idattivita=".$idattivita;
		$this->delete($where);
	}
	public function updateAttivita($data,$var) {
        $table = 'attivita'; //nome tabella
        $where = 'idattivita='.$var; 
        $this->update($data, $where, $table);
    }

}
