<?php

class Application_Resource_Prescrizionef extends Zend_Db_Table_Abstract {
	protected $_name = 'prescrizionef';
	protected $_primary = 'idter';

	public function getPresFarmaco($var){
		return $this -> find($var) -> current();
	}
	
	public function prescriviFarmaco($info){
		$this->insert($info);
	}
	
}
