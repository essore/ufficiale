<?php

class Application_Resource_Faq extends Zend_Db_Table_Abstract {
	protected $_name    = 'faq';
    protected $_primary  = 'id';
    protected $_rowClass = 'Application_Resource_Faq_Item';
    
	public function init()
    {
    }
	
	// Estrae i dati della categoria $id
    public function getFaq()
    {
       $select= $this->select();
	   return $this->fetchAll($select);
    }
	
	public function insertFaq($info){
		
		$this->insert($info);
	}
	
	public function cancellaFaq($id){
		$where = "id=".$id;
		$this->delete($where);
	}
	
	public function updateFaq($data, $id) {
        $table = 'faq'; 
        $where = 'id='.$id;
        $this->update($data, $where, $table);
    }
}

