<?php

class Application_Resource_Utente extends Zend_Db_Table_Abstract {
	protected $_name    = 'utente';
    protected $_primary  = 'idutente';
  //  protected $_rowClass = 'Application_Resource_Utente_Item';
    
	public function init()
    {
    }
	
	// Estrae i dati della categoria $idutente
    public function getUtenteById()
    {
       $select= $this->select();
	   return $this->fetchAll($select);
    }
	
	public function newutente($info)
	{
		return $this->insert($info);
	}
    public function getUtenteByName($usrName)
    {
        return $this->fetchRow($this->select()->where('username = ?', $usrName));
    }	
	public function getUtente($id){
		return $this->find($id)->current();
	}
	public function updateCredenziali($var, $id) {
        $table = 'utente'; 
        $where = 'idutente='.$id; 
        $this->update($var, $where, $table);
    }
	public function updateDirettore($data, $id){
		$tables='utente';
		$where='idutente = '.$id;
		$this->update($data, $where, $table);
		
	}
	public function getDirettori()
	{
		$where = 'ruolo = \'direttore\'';
		$select = $this-> select()
				->where($where);
		return $this->fetchAll($select); 
		
	}
}