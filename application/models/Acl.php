<?php 

class Application_Model_Acl extends Zend_Acl
{
	public function __construct()
	{
		// ACL per public
		$this->addRole(new Zend_Acl_Role('public'))
			 ->add(new Zend_Acl_Resource('public'))
			 ->add(new Zend_Acl_Resource('error'))
			 ->add(new Zend_Acl_Resource('index'))
			 ->allow('public', array('public','error','index'));
			 
		// ACL per paziente
		$this->addRole(new Zend_Acl_Role('paziente'), 'public')
			 ->add(new Zend_Acl_Resource('paziente'))
			 ->allow('paziente','paziente');
				   
		// ACL per clinico
		$this->addRole(new Zend_Acl_Role('clinico'), 'public')
			 ->add(new Zend_Acl_Resource('clinico'))
			 ->allow('clinico','clinico');
			
		// ACL per direttore
		$this->addRole(new Zend_Acl_Role('direttore'), 'clinico')
			 ->add(new Zend_Acl_Resource('direttore'))
			 ->allow('direttore','direttore');
	
		// ACL for admin
		$this->addRole(new Zend_Acl_Role('admin'), 'direttore')
			 ->add(new Zend_Acl_Resource('admin'))
			 ->allow('admin','admin');
	}
}