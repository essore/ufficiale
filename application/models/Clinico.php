<?php

class Application_Model_Clinico extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	/* public function getFaqById()   ***************da cambiare*
    {
        return $this->getResource('Faq')->getFaq();
    }
	
	
	
	
	 public function getUtenteById()
    {
        return $this->getResource('Utente')->getUtenteById();
    }
	 
	 */

	public function getUtente($id){
		
		return $this->getResource('Utente')->getUtente($id);
	}

	public function getUtenteByName($info){
		
		return $this->getResource('Utente')->getUtenteByName($info);
	}


	public function getClinico()
    {
        return $this->getResource('Clinico')->getClinico();
    }
	
	public function getClinicoById($id)
	{
		return $this->getResource('Clinico')->getClinicoById($id);
	}
	
	public function newutente($info)
	{
		return $this->getResource('Utente')->newutente($info);
	}
	public function newpaziente($info)
	{
		return $this->getResource('Paziente')->newpaziente($info);
	}
	public function getPazienteById($id)
	{
		return $this->getResource('Paziente')->getPaziente($id);
	}
	
	    public function getpazienti() 
    {
        return $this->getResource('Paziente')->getpazienti();
    }
	
	    public function getFaq() 
    {
        return $this->getResource('Faq')->getFaq();
    }
	
	public function updateClinico($var, $id){ // funzione che richiama /resources/Clinico
        return $this->getResource('Clinico')->updateClinico($var, $id);
    }
	
	public function mypaziente($var){
		return $this->getResource('Segue')->mypaziente($var);
	}
	public function newsegue($var){
		return $this->getResource('Segue')->newsegue($var);
	}
	public function notmypaziente($idp){
		return $this->getResource('Paziente')->notmypaziente($idp);
	}
	

	public function updateCredenziali($var, $id){ 
        return $this->getResource('Utente')->updateCredenziali($var, $id);
    }


	public function getDisturbo()
	{
		return $this->getResource('Disturbo')->getDisturbo();	
	}
	public function recuperaDisturbi($var){
		return $this->getResource('Disturbo')->recuperaDisturbi($var);	
	}
	public function getDisturbiDi($ids)
	{
        return $this->getResource('Patologia')->getDisturbiDi($ids);
	}
	public function setpatologia($info)
	{
		return $this->getResource('Patologia')->setpatologia($info);
	}
	public function cancellaPatologia($where)
	{
		return $this->getResource('Patologia')->cancellaPatologia($where);
	}
	public function esistePatologia($dist,$idp)
	{
		return $this->getResource('Patologia')->esistePatologia($dist,$idp);
	}
	public function cercapazienti($idpazienti, $nome, $wild)
	{
		return $this->getResource('Paziente')->cercapazienti($idpazienti, $nome , $wild);
	}
	public function recuperapazienti($idpazienti)
	{
		return $this->getResource('Paziente')->recuperapazienti($idpazienti);
	}
	public function addTerapia($var)
	{
		return $this->getResource('Terapia')->addTerapia($var);
	}
	
	public function getAllAttivita()
	{
		return $this->getResource('Attivita')->getAllAttivita();
	}
	
	public function getAttivitaById($var)
	{
		return $this->getResource('Attivita')->getAttivitaById($var);
	}

	public function getAllFarmaco()
	{
		return $this->getResource('Farmaco')->getAllFarmaco();
	}
	
	public function getFarmacoById($var)
	{
		return $this->getResource('Farmaco')->getFarmacoById($var);
	}
	
	public function prescriviAttivita($info)
	{
		return $this->getResource('Prescrizionea')->prescriviAttivita($info);
	}
	
	public function prescriviFarmaco($info)
	{
		return $this->getResource('Prescrizionef')->prescriviFarmaco($info);
	}
	
	public function getTerapiaId($var)
	{
		return $this->getResource('Terapia')->getTerapiaId($var);
	}

	public function esistesegue($idc, $idp)
	{
		return $this->getResource('Segue')->esistesegue($idc, $idp);
	}

	public function filtrapaziente($datapre, $datapost, $var)
	{
		return $this->getResource('Episodio')->filtrapaziente($datapre, $datapost, $var);
	}
	public function filtrapa($var, $where){
				return $this->getResource('Episodio')->filtrapa($var, $where);
	}
	
	public function getEpisodio($var)   
    {
        return $this->getResource('Episodio')->getEpisodio($var);
	}
	
	public function getdisturbobyid($var)
	{
		
		return $this->getResource('Disturbo')->getdisturbobyid($var);
	}	
		
	public function getTerapia($var)
	{
		return $this->getResource('Terapia')->getTerapia($var);
	}

	public function seguitoDa($var) // funzione per ricavare da quali clinici è seguito il paziente loggato
	{
		return $this->getResource('Segue')->seguitoDa($var);
	}	
	
		public function getAttivita($var)
	{
		return $this->getResource('Prescrizionea')->getAttivita($var);
	}
	
	public function getPresFarmaco($var)
	{
		return $this->getResource('Prescrizionef')->getPresFarmaco($var);
	}
}
