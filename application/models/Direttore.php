<?php

class Application_Model_Direttore extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
    public function getpazienti() /* da eliminare e usare guest sul public controller*/
    {
        return $this->getResource('Paziente')->getpazienti();
    }
	
	public function saveDisturbo($pippo){ //funzione anche del direttore
		return $this->getResource('Disturbo')->insertDisturbo($pippo);
	}
	
	public function getDisturbo()   
    {
        return $this->getResource('Disturbo')->getDisturbo();
		
    }
	
	public function cancellaDisturbo($iddisturbo)
	{
		return $this->getResource('Disturbo')->cancellaDisturbo($iddisturbo);
	}	
	
	public function get($id)
	{
		return $this->getResource('Segue')->get($id);
	}
	public function getClinico()
    {
        return $this->getResource('Clinico')->getClinico();
    }
		
	public function takeallsegue()
	{
		return $this->getResource('Segue')->takeall();
	}
	public function recuperaclinici($id){
		return $this->getResource('Clinico')->recuperaclinici($id);
	}
	public function recuperapazienti($id)
	{
		return $this->getResource('Paziente')->recuperapazienti($id);
	}
	public function recuperasegue($id)
	{
		return $this->getResource('Segue')->recuperasegue($id);
	}
	public function filtrapazienti($where)
	{
		return $this->getResource('Paziente')->filtrapazienti($where);
		
	}
	public function filtradisturbi($dist, $idp)
	{
		return $this->getResource('Patologia')->filtradisturbi($dist, $idp);	
	}

	public function getFarmaci(){
			return $this->getResource('farmaco')->getFarmaci();
		}
		
	public function getAttivita(){
			return $this->getResource('attivita')->getAttivita();
		}
	public function saveAttivita($values){ //funzione anche del direttore
		return $this->getResource('Attivita')->insertAttivita($values);
	}
		public function cancellaAttivita($idattivita)
	{
		return $this->getResource('Attivita')->cancellaAttivita($idattivita);
		
	}
	public function saveFarmaco($values){ //funzione anche del direttore
		return $this->getResource('farmaco')->insertFarmaco($values);
	}
		public function cancellaFarmaco($idfarmaco)
	{
		return $this->getResource('Farmaco')->cancellaFarmaco($idfarmaco);
		
	}
	public function eliminasegue($where){
		return $this->getResource('Segue')->eliminasegue($where);
	}
	public function cercapazienti($idpazienti, $nome, $wild)
	{
		return $this->getResource('Paziente')->cercapazienti($idpazienti, $nome , $wild);
	}
	
	public function updateDisturbo($data,$var){
		return $this->getResource('disturbo')->updateDisturbo($data,$var);
	}
	public function updateFarmaco($data,$var){
		return $this->getResource('farmaco')->updateFarmaco($data,$var);
	}
	public function updateAttivita($data,$var){
		return $this->getResource('attivita')->updateAttivita($data,$var);
	}
	public function getPazienteById($id)
	{
		return $this->getResource('Paziente')->getPaziente($id);
	}

	public function seguitoDa($var) // funzione per ricavare da quali clinici è seguito il paziente loggato
	{
		return $this->getResource('Segue')->seguitoDa($var);
	}
	public function getClinicoById($id)
	{
		return $this->getResource('Clinico')->getClinicoById($id);
	}
	public function getclinicibyRuolo($ruolo)
	{
		return $this->getResource('Clinico')-> getclinicibyRuolo($ruolo);	
	}
	public function newsegue($info)
	{
		return $this->getResource('Segue')-> newsegue($info);
	}
	public function esistesegue($idc, $idp)
	{
		return $this->getResource('Segue')->esistesegueb($idc, $idp);
	}
}