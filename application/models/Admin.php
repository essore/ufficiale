<?php

class Application_Model_Admin extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
	public function saveFaq($info)
    {
        return $this->getResource('Faq')->insertFaq($info);
    }
	
	public function cancellafaq($id)
	{
		return $this->getResource('Faq')->cancellaFaq($id);
		
	}
	 public function getFaqById() 
    {
        return $this->getResource('Faq')->getFaq();
    }

	public function updateFaq($data, $id){ 
        return $this->getResource('Faq')->updateFaq($data, $id);
    }
	public function getNrClinici(){
		return $this->getResource('clinico')->getNrClinici();
	}
	public function getNrPazienti(){
		return $this->getResource('paziente')->getNrPazienti();
	}
	public function getNrEventi(){
		return $this->getResource('episodio')->getNrEventi();
	}
	
	public function getClinico()
    {
        return $this->getResource('Clinico')->getClinico();
    }
	
	public function getUtenteByName($info){
		
		return $this->getResource('Utente')->getUtenteByName($info);
	}

	public function newutente($info)
	{
		return $this->getResource('Utente')->newutente($info);
	}
	
	public function newclinico($info)
	{
		return $this->getResource('Clinico')->newclinico($info);
	}	
	public function cancellaClinico($riga){
		return $this->getResource('clinico')->cancellaClinico($riga);
	}
	public function getClinicoById($id){
		return $this->getResource('clinico')->getClinicoById($id);
	}
	public function updateClinicoDati($data, $id){
		return $this->getResource('clinico')->updateClinico($data,$id);
	}
	public function updateDirettore($data, $id){
		return $this->getResource('utente')->updateDirettore($data, $id);
	}
	public function getDirettori(){
		return $this->getResource('utente')->getDirettori();
	}
	public function recuperaclinici($id){
		return $this->getResource('Clinico')->recuperaclinici($id);
	}
	public function filtrapaziente($datapre, $datapost, $var){
		return $this->getResource('terapia')->filtrapaziente($datapre, $datapost, $var);
	}
	public function getPazienti()
    {
        return $this->getResource('paziente')->getpazienti();
    }
	public function savePazienti($pippo){ 
		return $this->getResource('paziente')->savePaziente($pippo);
	}
	public function cancellapaziente($idpaziente)
	{
		return $this->getResource('paziente')->cancellaPaziente($idpaziente);
	}	
	public function updatePaziente($data,$var){
		return $this->getResource('paziente')->updatePaziente($data,$var);
	}
	public function getDisturbo()   
    {
        return $this->getResource('Disturbo')->getDisturbo();
		
    }
	
	public function getNrEventiFiltroq1($datapre, $datapost){
		return $this->getResource('episodio')->getNrEventiFiltroq1($datapre, $datapost);
	}
	public function getNrEventiFiltroq2($datapre, $datapost, $intensita){
		return $this->getResource('episodio')->getNrEventiFiltroq2($datapre, $datapost, $intensita);
	}
	public function getNrEventiFiltroq3($datapre, $datapost, $disturbo){
		return $this->getResource('episodio')->getNrEventiFiltroq3($datapre, $datapost, $disturbo);
	}
	public function getNrEventiFiltroq4($datapre, $datapost, $intensita, $disturbo){
		return $this->getResource('episodio')->getNrEventiFiltroq4($datapre, $datapost, $intensita, $disturbo);
	}
	
}