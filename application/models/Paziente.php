<?php

class Application_Model_Paziente extends App_Model_Abstract {
	
		public function __construct()
    {
		$this->_logger = Zend_Registry::get('log');  	
	}
	
	
    public function getPaziente($var)
    {
        return $this->getResource('Paziente')->getPaziente($var);
    }
	public function saveEpisodio($pippo){ //funzione anche del direttore
		return $this->getResource('Episodio')->insertEpisodio($pippo);
	}
	
	public function getEpisodio($var)   
    {
        return $this->getResource('Episodio')->getEpisodio($var);
	}
	
	public function updateEpisodio($data, $var)
	{
	    return $this->getResource('Episodio')->updateEpisodio($data, $var);
	}
	
	public function cancellaEpisodio($idepisodio)
	{
		return $this->getResource('Episodio')->cancellaEpisodio($idepisodio);
	}	

	public function cancellatuttiEpisodi($id)
	{
		return $this->getResource('Episodio')->cancellatuttiEpisodi($id);
	}
	
	public function cancellaepisodifiltrati($id, $datapre, $datapost){
		return $this->getResource('Episodio')->cancellaepisodifiltrati($id, $datapre, $datapost);
	}
	/*public function getDisturbo()
	{
		
		return $this->getResource('Disturbo')->getDisturbo();
		
	}	*/
	public function getdisturbobyid($var)
	{
		
		return $this->getResource('Disturbo')->getdisturbobyid($var);
	}	
	
	public function getDisturbiDi($var)
	{
		
		return $this->getResource('Patologia')->getDisturbiDi($var);
	}
	
	public function seguitoDa($var) // funzione per ricavare da quali clinici è seguito il paziente loggato
	{
		return $this->getResource('Segue')->seguitoDa($var);
	}	
	
	public function getClinicoById($var) // per ricavare il nome del clinico dall'id
	{
		return $this->getResource('Clinico')->getClinicoById($var);
	}
	
	public function getTerapia($var)
	{
		return $this->getResource('Terapia')->getTerapia($var);
	}
	
	public function getAttivita($var)
	{
		return $this->getResource('Prescrizionea')->getAttivita($var);
	}
	
	public function getPresFarmaco($var)
	{
		return $this->getResource('Prescrizionef')->getPresFarmaco($var);
	}
	
	public function getAttivitaById($var)
	{
		return $this->getResource('Attivita')->getAttivitaById($var);
	}
	
	public function getFarmacoById($var)
	{
		return $this->getResource('Farmaco')->getFarmacoById($var);
	}	
	public function getUtente($id){
		
		return $this->getResource('Utente')->getUtente($id);
	}
	public function updateCredenziali($var, $id){ 
        return $this->getResource('Utente')->updateCredenziali($var, $id);
    }

	public function getUtenteByName ($usrName){
		return $this->getResource('Utente')->getUtenteByName($usrName);
	}

	
	public function updatePaziente($var, $id){
		return $this->getResource('Paziente')->updatePaziente($var, $id);
    }

	
	public function filtrapaziente($datapre, $datapost, $var){
		return $this->getResource('Episodio')->filtrapaziente($datapre, $datapost, $var);
	}
		
    public function getFaq() 
    {
        return $this->getResource('Faq')->getFaq();
    }
	
	public function setNotifica($id)
	{
		return $this->getResource('Terapia')->setNotifica($id);
	}

}
	
	