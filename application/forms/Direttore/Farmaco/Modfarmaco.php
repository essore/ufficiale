<?php
class Application_Form_Direttore_Farmaco_Modfarmaco extends App_Form_Abstract
{
	
	
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('modfarmaco');
		$this->setAction('');
		
		
		
		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'nome farmaco',
            'filters' => array('LocalizedToNormalized'),
       'decorators' => $this->elementDecorators,
            ));
			
        

        $this->addElement('submit', 'Aggiorna', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'modifica',
            'decorators' => $this->elementDecorators,
            ));
    
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
	
	}
	
}
	