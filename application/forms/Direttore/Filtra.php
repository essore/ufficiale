<?php
class Application_Form_Direttore_Filtra extends App_Form_Abstract
{
	protected $_direttoreModel;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('filtra');
		$this->setAction('');
		$_direttoreModel= new Application_Model_Direttore;
		
		
		
		$this->addElement('select', 'range', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
     		'required'   => FALSE,
   		    'multiOptions' => array(
   		    			'='=>'uguale a',
						'<'=>'minore di',
						'>'=>'maggiore di',			
				),
     		'label'      => 'eta',
            'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('text', 'eta', array(
            'validators' => array( array('int',TRUE)),
            'required'   => FALSE,
            'label'      => 'Eta',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            'label'      => '',
        ));
		
		$this->addElement('radio', 'genere', array(
		    'label'=>'Sesso',
		    'multiOptions'=>array(
		        'maschio' => 'maschio',
		        'femmina' => 'femmina',
		    ),
		    'decorators' => $this->elementDecorators,
		));				
		
		$disturbi = $_direttoreModel->getDisturbo();
		$opzioni=array('0'=>'nessun filtro');
		foreach($disturbi as $disturbo){
			$opzioni+=array($disturbo->iddisturbo =>$disturbo->nome);			
		}
		
		$this->addElement('select', 'disturbo', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => FALSE,
   		    'multiOptions' => $opzioni,
     		'label'      => 'disturbo',
   	     'decorators' => $this->elementDecorators,	
        ));
        $this->addElement('submit', 'filtra', array(
            //'required' => false,
           // 'ignore' => TRUE,
            'decorators' => $this->elementDecorators,
            ));	
		
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));


    }
}
	