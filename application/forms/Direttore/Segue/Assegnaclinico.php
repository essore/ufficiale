<?php
class Application_Form_Direttore_Segue_Assegnaclinico extends App_Form_Abstract
{
	protected $_direttoreModel;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('assegnaclinico');
		$this->setAction('');
		
		$this->_direttoreModel= new Application_Model_Direttore();		
		/*
		$this->addElement('radio', 'ruolo', array(
		    'label'=>'Ruolo',
		    'multiOptions'=>array(
		        'medico' => 'medico',
		        'fisioterapista' => 'fisioterapista',
		    ),
		    'decorators' => $this->elementDecorators,
		));		
		*/
		
		
		$clinici = $this->_direttoreModel->getClinico();	
		$elementi=array();
		foreach($clinici as $x){					
			$elementi+=array($x->idclinico => $x->cognome.' '.$x->nome);						
		}
	
		$this->addElement('select', 'clinico', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => true,
   		    'multiOptions' => $elementi,
     		'label'      => 'seleziona il clinico',
	        'decorators' => $this->elementDecorators,
        ));
					
		
        $this->addElement('submit', 'assegnaclinico', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'assegna',
      	    'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));		
	}
}