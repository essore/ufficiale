<?php
class Application_Form_Paziente_Modprof extends App_Form_Abstract
{
	
	protected $_pazienteModel;
	protected $_authService;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('modprof');
		$this->setAction('');
		$this->_pazienteModel = new Application_Model_Paziente();
        $this->_authService = new Application_Service_Auth();
		

		$id = $this->_authService->getIdentity()->idutente;
		$var = $this->_pazienteModel->getPaziente($id); 
		
		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->nome,
            'label'      => 'nome',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->cognome,
            'label'      => 'cognome',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('text', 'eta', array(
            'validators' => array( array('int',TRUE)),
            'required'   => true,
            'value'      => $var->eta,
            'label'      => 'Eta',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('radio', 'genere', array(
		    'label'=>'Sesso',
            'value'      => $var->genere,
		    'multiOptions'=>array(
		    'maschio' => 'maschio',
		    'femmina' => 'femmina',
		    'misto' => 'poco',
		    ),
		));
		
		$this->addElement('text', 'indirizzo', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => FALSE,
            'value'      => $var->indirizzo,
            'label'      => 'Indirizzo',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('text', 'telefono', array(
            'validators' => array( array('int',TRUE)),
            'required'   => FALSE,
            'value'      => $var->telefono,
            'label'      => 'telefono',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));

		$this->addElement('text', 'email', array(
		    'filters'    => array('StringTrim', 'StringToLower'),
		    'validators' => array(
		        array('StringLength', TRUE, array(3, 32)),
				array('EmailAddres', true)
		    ),
		    'required'   => FALSE,
            'value'      => $var->email,
		    'label'      => 'email',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
		));

        $this->addElement('submit', 'Aggiorna', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));

		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
    }
}
	