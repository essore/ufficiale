<?php
class Application_Form_Paziente_Episodio_Add extends App_Form_Abstract
{
	protected $_pazienteModel;
	protected $_authService;
	protected $_logger;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addepisodio');
		$this->setAction('');
		$_pazienteModel= new Application_Model_Paziente;
		$this->_authService = new Application_Service_Auth();	
		$this->_logger = Zend_Registry::get('log');  	
		
		
		
		$mioid = $this->_authService->getIdentity()->idutente;
		$mieidisturbi = $_pazienteModel->getDisturbiDi($mioid);	
		$opzioni=array();
		
		foreach($mieidisturbi as $x){					
			$num=$x->iddisturbo;					
			$dolore = $_pazienteModel->getdisturbobyid($num);
			$opzioni+=array($num => $dolore->nome);		
		}
		$this->addElement('select', 'disturbo', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => true,
   		    'multiOptions' => $opzioni,
     		'label'      => 'disturbo',
            'decorators' => $this->elementDecorators,
        ));

        $this->addElement('text', 'data', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                			array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'data',
            'decorators' => $this->elementDecorators,
			));
			
		$this->addElement('select', 'ora', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'),
            'required'   => true,
            'label'      => 'ora',     
            'decorators' => $this->elementDecorators,       
            ));
						
		$this->addElement('select', 'minuti', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => true,
            'label'      => 'minuti',     
            'decorators' => $this->elementDecorators,       
            ));  
			
        $this->addElement('select', 'secondi', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => true,
            'label'      => 'secondi',     
            'decorators' => $this->elementDecorators,       
            ));
            
            
            
  		$this->addElement('text', 'durata', array(
            'validators' => array('Int'),
            'required'   => true,
            'label'      => 'durata in secondi',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));
			
			
		$this->addElement('select', 'intensita', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10'),
            'required'   => true,
            'label'      => 'intensita',
            'decorators' => $this->elementDecorators,
            ));
			

         $this->addElement('submit', 'Episodio', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));
    
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));    

	}
}