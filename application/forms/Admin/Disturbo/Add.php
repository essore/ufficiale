<?php
class Application_Form_Admin_Disturbo_Add extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addditurbo');
		$this->setAction('');

		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'nome disturbo',
            'filters' => array('LocalizedToNormalized'),
       'decorators' => $this->elementDecorators,
            ));
			
        

        $this->addElement('submit', 'Disturbo', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));
    
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
	
	}
	
}
	