<?php
class Application_Form_Admin_Paziente_Modificapaziente extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addpaziente');
		$this->setAction('');

		$this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'username',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'password', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'		 => 'pass',
            'label'      => 'password',
            'filters' => array('LocalizedToNormalized'),
			'decorators' => $this->elementDecorators,
            ));
        
        $this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Nome',
            'filters' => array('LocalizedToNormalized'),
      		'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Cognome',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('text', 'eta', array(
            'validators' => array( array('int',TRUE)),
            'required'   => true,
            'label'      => 'Eta',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('radio', 'genere', array(
		    'label'=>'Sesso',
		    'multiOptions'=>array(
		    'maschio' => 'maschio',
		    'femmina' => 'femmina',
		    ),
       		'decorators' => $this->elementDecorators,
			
		));
		
		$this->addElement('text', 'indirizzo', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => FALSE,
            'label'      => 'Indirizzo',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('text', 'telefono', array(
            'validators' => array( array('int',TRUE)),
            'required'   => FALSE,
            'label'      => 'telefono',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));

		$this->addElement('text', 'email', array(
		    'filters'    => array('StringTrim', 'StringToLower'),
		    'validators' => array(
		        array('StringLength', TRUE, array(3, 32)),
				array('EmailAddres', true)
		    ),
		    'required'   => FALSE,
		    'label'      => 'email',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
		));
		
		$this->addElement('radio', 'associa', array(
		    'label'=>'E\' un tuo paziente?',
		    'multiOptions'=>array(
		        TRUE => 'si',
		        FALSE  => 'no',
		    ),
		    'decorators' => $this->elementDecorators,
			'value' => TRUE,
		));

        $this->addElement('submit', 'paziente', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
    }
}
	