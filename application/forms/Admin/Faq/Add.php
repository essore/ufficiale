<?php
class Application_Form_Admin_Faq_Add extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addfaq');
		$this->setAction('');

		$this->addElement('text', 'domanda', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'domanda',
            'filters' => array('LocalizedToNormalized'),
     		'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'risposta', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'risposta',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
        

        $this->addElement('submit', 'Faq', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));
			
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
    }
}
	