<?php
class Application_Form_Admin_Faq_Upd extends App_Form_Abstract
{
//	protected $_adminModel;
//al momento non è usata... cambiare metodo  updateFaqForm	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('updatefaq');
		$this->setAction('');
		
		/*capire come inviare parametro verso form
		$_adminModel=new Application_Model_Admin();
		
		
		$domanda= $this->_getParam('dom');//incredibilmente funziona!!
        $id = $_adminModel->getFaqById($domanda);
*/
		$this->addElement('text', 'domanda', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'domanda',
            'filters' => array('LocalizedToNormalized'),
      		'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'risposta', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'risposta',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
        

        $this->addElement('submit', 'Faq', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
			));
    
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));

}
}