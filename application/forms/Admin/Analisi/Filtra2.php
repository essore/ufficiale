<?php
class Application_Form_Admin_Analisi_Filtra2 extends App_Form_Abstract
{
		public function init()
	{
		$_adminModel= new Application_Model_Admin;
		$this->setMethod('post');
		$this->setName('filtra2');
		$this->setAction('');
	
	
		$disturbi = $_adminModel->getDisturbo();
		$opzioni=array('0'=>'nessun filtro');
		foreach($disturbi as $disturbo){
			$opzioni+=array($disturbo->iddisturbo =>$disturbo->nome);			
		}
		
		$this->addElement('select', 'disturbo', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => FALSE,
   		    'multiOptions' => $opzioni,
     		'label'      => 'disturbo',
   	     'decorators' => $this->elementDecorators,	
        ));
		$this->addElement('select', 'intensita', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10'),
            'required'   => false,
            'label'      => 'intensita',
            'decorators' => $this->elementDecorators,
            ));
	 $this->addElement('text', 'datapre2', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => true,
            'label'      => 'data di partenza',
            'decorators' => $this->elementDecorators,
			));
			
		$this->addElement('select', 'orapre2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'),
            'required'   => false,
            'label'      => 'ora di partenza',     
            'decorators' => $this->elementDecorators,       
            ));
						
		$this->addElement('select', 'minutipre2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => false,
            'label'      => 'minuti di partenza',     
            'decorators' => $this->elementDecorators,       
            ));  
			
        $this->addElement('select', 'secondipre2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => false,
            'label'      => 'secondi di partenza',     
            'decorators' => $this->elementDecorators,       
            ));
            
            
	 $this->addElement('text', 'datapost2', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 50))
            ),
            'required'   => false,
            'label'      => 'data di fine',
            'decorators' => $this->elementDecorators,
			));
			
		$this->addElement('select', 'orapost2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'),
            'required'   => false,
            'label'      => 'ora di fine',     
            'decorators' => $this->elementDecorators,       
            ));
						
		$this->addElement('select', 'minutipost2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => false,
            'label'      => 'minuti di fine',     
            'decorators' => $this->elementDecorators,       
            ));  
			
        $this->addElement('select', 'secondipost2', array(
 			'multiOptions' => array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52','53','54','55','56','57','58','59'),
            'required'   => true,
            'label'      => 'secondi di fine',     
            'decorators' => $this->elementDecorators,       
            ));
            
			
       $this->addElement('submit', 'Filtra2', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'Filtra',
            'decorators' => $this->elementDecorators,
            ));
    
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        )); 
	}
	
}