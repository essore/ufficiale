<?php
class Application_Form_Admin_Clinico_Add extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('addclinico');
		$this->setAction('');

		$this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'username',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'password', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'		 => 'pass',
            'label'      => 'password',
            'filters' => array('LocalizedToNormalized'),
			'decorators' => $this->elementDecorators,
            ));
        
        $this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Nome',
            'filters' => array('LocalizedToNormalized'),
      		'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Cognome',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
		
		$this->addElement('radio', 'ruolo', array(
		    'label'=>'Ruolo',
		    'multiOptions'=>array(
		    	'medico' => 'medico',
			    'required' => true,
		   		'fisioterapista' => 'fisioterapista',
      		),
      		'decorators' => $this->elementDecorators,
		));
		
			
		$this->addElement('text', 'specializzazione',  array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Specializzazione',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
        $this->addElement('submit', 'clinico', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'aggiungi',
            'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
    }
}
	