<?php
class Application_Form_Admin_Clinico_Modifica extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('modificaclinico');
		$this->setAction('');

		$this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'username',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'password', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'		 => 'pass',
            'label'      => 'password',
            'filters' => array('LocalizedToNormalized'),
			'decorators' => $this->elementDecorators,
            ));
        
        $this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Nome',
            'filters' => array('LocalizedToNormalized'),
      		'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Cognome',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
            ));
		
		$this->addElement('radio', 'ruolo', array(
		    'label'=>'Ruolo',
		    'required'   => true,
		    'multiOptions'=>array(
		    	'medico' => 'medico',
		   		'fisioterapista' => 'fisioterapista',
      		),
      		'decorators' => $this->elementDecorators,
		));
		
			
		$this->addElement('text', 'specializzazione',  array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'label'      => 'Specializzazione',
            'filters' => array('LocalizedToNormalized'),
       		'decorators' => $this->elementDecorators,
        ));
		
        $this->addElement('submit', 'clinico', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'modifica',
            'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
    }
}
	