<?php
class Application_Form_Clinico_Terapia_Add extends App_Form_Abstract
{
	protected $_clinicoModel;
	
	public function init()
	{
		$this->_clinicoModel = new Application_Model_Clinico();
		$this->setMethod('post');
		$this->setName('newterapia');
		$this->setAction('');

		$attivita = $this->_clinicoModel->getAllAttivita();	
		$opzioni=array();
		
		foreach($attivita as $x){					
			$num=$x->idattivita;					
			$att = $this->_clinicoModel->getAttivitaById($num);
			$opzioni+=array($num => $att->nome);		
		}
		$this->addElement('select', 'attivita', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => true,
   		    'multiOptions' => $opzioni,
     		'label'      => 'attivita',
            'decorators' => $this->elementDecorators,
        ));
		
		$this->addElement('text', 'durata', array(
            'validators' => array( array('int',TRUE)),
            'required'   => true,
            'label'      => 'durata',
       'decorators' => $this->elementDecorators,
            ));
		
		$farmaco = $this->_clinicoModel->getAllFarmaco();	
		$opzioni=array();
		
		foreach($farmaco as $x){					
			$num=$x->idfarmaco;					
			$farm = $this->_clinicoModel->getFarmacoById($num);
			$opzioni+=array($num => $farm->nome);		
		}
		$this->addElement('select', 'farmaco', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => false,
   		    'multiOptions' => $opzioni,
     		'label'      => 'farmaco',
            'decorators' => $this->elementDecorators,
        ));

        $this->addElement('text', 'dosaggio', array(
            'validators' => array( array('int',TRUE)),
            'required'   => false,
            'label'      => 'dosaggio',
       'decorators' => $this->elementDecorators,
            ));

        $this->addElement('submit', 'terapia', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'prescrivi',
            'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
    }
}
	