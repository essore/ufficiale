<?php
class Application_Form_Clinico_Updatepass extends App_Form_Abstract
{
	
	protected $_clinicoModel;
	protected $_authService;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('updatepass');
		$this->setAction('');
		$this->_clinicoModel = new Application_Model_Clinico();
        $this->_authService = new Application_Service_Auth();
		

		$id = $this->_authService->getIdentity()->idutente;
		$var = $this->_clinicoModel->getUtente($id); 
		
		$this->addElement('text', 'username', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->username,
            'label'      => 'username',
           'decorators' => $this->elementDecorators,
            ));
			
			
			$this->addElement('text', 'password', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->password,
            'label'      => 'password',
           'decorators' => $this->elementDecorators,
            ));
			
			 $this->addElement('submit', 'Modifica', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'modifica',
            'decorators' => $this->elementDecorators,
            ));

		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
			
	}
}
