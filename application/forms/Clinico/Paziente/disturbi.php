<?php
class Application_Form_Clinico_Paziente_Disturbi extends App_Form_Abstract
{
	protected $_clinicoeModel;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('disturbi');
		$this->setAction('');
		$_clinicoModel= new Application_Model_Clinico;
		
		$disturbi = $_clinicoModel->getDisturbo();
		$opzioni=array();
		foreach($disturbi as $disturbo){
			$opzioni+=array($disturbo->iddisturbo =>$disturbo->nome);			
		}
		
		$this->addElement('select', 'disturbo', array(
       	'filters'    => array('StringTrim', 'StringToLower'),
       		'validators' => array('Int'),
     		'required'   => FALSE,
   		    'multiOptions' => $opzioni,
     		'label'      => 'disturbo',
   	     'decorators' => $this->elementDecorators,	
        ));
		$this->addElement('submit', 'paziente', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'assegna',
            'decorators' => $this->elementDecorators,
            ));	
		
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
  }
}