<?php
class Application_Form_Clinico_Paziente_Search extends App_Form_Abstract
{
	public function init()
	{
		$this->setMethod('post');
		$this->setName('search');
		$this->setAction('');

		$this->addElement('text', 'nome', array(
            'required'   => false,
            'label'      => 'Cerca il paziente',
           'filters' => array('StringTrim'),
       		'decorators' => $this->elementDecorators,
            ));
			
		$this->addElement('submit', 'paziente', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'filtra',
            'decorators' => $this->elementDecorators,
            ));
			
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
		
    }
}