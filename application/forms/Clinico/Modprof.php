<?php
class Application_Form_Clinico_Modprof extends App_Form_Abstract
{
	
	protected $_clinicoModel;
	protected $_authService;
	
	public function init()
	{
		$this->setMethod('post');
		$this->setName('modprof');
		$this->setAction('');
		$this->_clinicoModel = new Application_Model_Clinico();
        $this->_authService = new Application_Service_Auth();
		

		$id = $this->_authService->getIdentity()->idutente;
		$var = $this->_clinicoModel->getClinicoById($id); 
		
		$this->addElement('text', 'nome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->nome,
            'label'      => 'nome',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));
			
			$this->addElement('text', 'cognome', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->cognome,
            'label'      => 'cognome',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));
			$this->addElement('radio', 'ruolo', array(
		    'label'=>'ruolo',
		    'value'      => $var->ruolo,
		    'required'   => true,
		    'multiOptions'=>array(
		    'medico' => 'medico',
		    'fisioterapista' => 'fisioterapista',
		    ),
       		'decorators' => $this->elementDecorators,
			
		));
			
			$this->addElement('text', 'specializzazione', array(
            'filters'    => array('StringTrim', 'StringToLower'),
            'validators' => array(
                array('StringLength', TRUE, array(3, 32))
            ),
            'required'   => true,
            'value'      => $var->specializzazione,
            'label'      => 'specializzazione',
            'filters' => array('LocalizedToNormalized'),
            'decorators' => $this->elementDecorators,
            ));

        $this->addElement('submit', 'Aggiorna', array(
            'required' => false,
            'ignore' => TRUE,
            'label'    => 'modifica',
            'decorators' => $this->elementDecorators,
            ));

		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'zend_form')),
        	array('Description', array('placement' => 'prepend', 'class' => 'formerror')),
            'Form'
        ));
    }
}
	