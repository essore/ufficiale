<?php

class AdminController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_adminModel;
	protected $_authService;
	protected $_form;	
	protected $_formcli;
	protected $_formfiltra;	
	protected $_filtraform2;
	protected $_formpazienti;
	protected $_updcliform;

	
    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');  
		$this->_authService = new Application_Service_Auth();
   		$this->_adminModel = new Application_Model_Admin();
		$this->view->faqForm = $this->getFaqForm();	
		$this->view->updfaqForm = $this->updateFaqForm();
		$this->view->updclinicoForm = $this->updateClinicoForm();
		$this->view->addclinicoForm = $this->getClinicoForm();
		$this->view->filtraform = $this->getfiltraform();
		$this->view->filtraform2 = $this->getfiltraform2();
		$this->view->updatepaziente = $this->getUpdatepazientiForm();
         }
		public function indexAction(){}
		
/******************************************************************************faq******************/

	private function getFaqForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Admin_Faq_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'addFaq'),
					'default',
					true
					));
			return $this->_form;
	}
		
	public function addfaqAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('newfaq');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newfaq');
		}
		$values = $form->getValues();
		$this->_adminModel->saveFaq($values);
		$this->_helper->redirector('faq'); /* rimanda alla action di admin controller*/
 	}
	public function newfaqAction(){ }
		
	public function cancfaqAction(){			
		$riga = $this->_getParam('dom');
		$this->_adminModel->cancellafaq($riga);
		$this->_helper->redirector('faq');
	}
		
	public function faqAction(){
		$test=$this->_adminModel->getFaqById();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	
	
	public function modificafaqAction(){   
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('main');
		}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
				return $this->render('updatefaq');
		}
		
		$domanda= $this->_getParam('dom');//incredibilmente funziona!!
        $id = $this->_adminModel->getFaqById($domanda);
 		$values = $this->_form->getValues();
		
        $data = array(  
                        'domanda' =>$values['domanda'],
                        'risposta' =>$values['risposta'],
                        );
		
		/*$data = array(  
                        'domanda' =>'eros',
                        'risposta' =>'ha modificato la faq con id'.$domanda,
                        );	*/			
						
		//$this->_logger->info('domanda = ');
        $this->_adminModel->updateFaq($data, $domanda);
        $this->_helper->redirector('faq');
	}
	
	

	public function updatefaqAction(){
			}
	
	private function updateFaqForm () {
		$urlHelper = $this->_helper->getHelper('url');
		$this->_form = new Application_Form_Admin_Faq_Add();
		$this->_form->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'modificafaq'
					),
					'default'
					));
		return $this->_form;
	}
		
	

    
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	   	
	
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}
	

	public function analisiAction(){  //la vista di tutti i clinici
	$test=$this->_adminModel->getNrClinici();
	$prova=count($test);
	
	$test1=$this->_adminModel->getNrPazienti();
	$prova2=count($test1);
	
	$analisi1= $prova2/$prova;
	$this->view->assign('prova',$analisi1);
	
	
	$test2=$this->_adminModel->getNrEventi();
	$prova3=count($test2);
	
	$analisi2= $prova3/$prova2;
	$this->view->assign('prova2',$analisi2);
	
	$query3= $this->_getparam('query3',null);
	$da2= $this->_getparam('da2',null);
	$a2= $this->_getparam('a2',null);
	$this->view->assign(array(
		'query3'=>$query3,
		'da2'=>$da2,
		'a2'=>$a2
	));
	
	$nrterapie= $this->_getparam('nrterapie',null);
	$da= $this->_getparam('da',null);
	$a= $this->_getparam('a',null);
	$this->view->assign(array(
		'nrterapie'=>$nrterapie,
		'da'=>$da,
		'a'=>$a
	));
	
	}

	public function personaleAction(){
		
		$test=$this->_adminModel->getClinico();
		
		$dir=$this->_adminModel->getDirettori();
		$dnome=array();
		$dcognome=array();
		$druolo=array();
		$dspecializzazione=array();
		
		$ids =array(''=>'');
		foreach($dir as $x){
			$ids += array($x->idutente => $x->idutente);
		}
		$direttori=$this->_adminModel->recuperaclinici($ids);
		
		$this->view->assign(array(
						'riga'=>$test,
						'direttori'=>$direttori
						));
}

		
	public function addclinicoAction() 	{}
	
	private function getClinicoForm(){
			$urlHelper = $this->_helper->getHelper('url');
			$this->_formcli = new Application_Form_Admin_Clinico_Add();
			$this->_formcli->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'newclinico'), 
					'default',
					true
					));
			return $this->_formcli;	
	}

public function newclinicoAction(){
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('addclinico');
			}
		$form=$this->_formcli;
		if (!$form->isValid($_POST)) {
			return $this->render('addclinico');
		}
		//con una sola form vogliamo fare l'insert in 2 tabelle distinte, attraverso gli array associativi
		//possiamo separarei i dati e fare l'insert prima nella tabella user poi nella tabella clinici.
		
		$values = $form->getValues(); //recupero dati dalla form
		
		
		if(!is_null($this->_adminModel->getUtenteByName($values['username']))){ //controllo se l'username richiesto è già presente
			$form->setDescription('username già occupato');
			return $this->render('addclinico');	
		}

		$user = array(	'username' =>$values['username'],
						'password' =>$values['password'],
						'ruolo'  =>'clinico'
						);
		$id= $this->_adminModel->newutente($user); //insert nella tabella utenti e ritorna 
		$clinico = array(
						'idclinico' => $id,	
						'nome' =>$values['nome'],
						'cognome' =>$values['cognome'],
						'ruolo' =>$values['ruolo'],
						'specializzazione' =>$values['specializzazione'],
						);
		
		$this->_adminModel->newclinico($clinico);

		
		$this->_helper->redirector('index'); 
		 /* rimanda alla visualizzazione del paziente*/
		/*'clinicobyid/id/'.$id -> stampa: http://localhost/ritenta/public/index.php/clinico/clinicobyid%2Fid%2F35*/
	}
	public function cancellaclinicoAction(){			
		$riga = $this->_getParam('clinico');
		$this->_adminModel->cancellaClinico($riga);
		$this->_helper->redirector('personale');
	}

	public function updateclinicoAction(){
			}
	
	public function modificaclinicoAction(){   
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('updateclinico');
		}
		$form=$this->_updcliform;
		if (!$form->isValid($_POST)) {
				return $this->render('updateclinico');
		}
		
		$id= $this->_getParam('clinico');
 		$values = $form->getValues();
		
        $data = array(  
                        'nome' =>$values['nome'],
                        'cognome' =>$values['cognome'],
                        'ruolo'=>$values['ruolo'],
                        'specializzazione'=>$values['specializzazione'],
                        );
		
        $this->_adminModel->updateClinicoDati($data, $id);
        $this->_helper->redirector('personale');
	}
	private function updateClinicoForm () {
		$urlHelper = $this->_helper->getHelper('url');
		$this->_updcliform = new Application_Form_Admin_Clinico_Modifica();
		$this->_updcliform->setAction($urlHelper->url(array(
					'controller' => 'admin',
					'action' => 'modificaclinico',
					),
					'default'
					));
		return $this->_updcliform;
	}
	public function nuovodirettoreAction(){
		
		$id= $this->_getParam('direttore');
        $data = array(  
        				
                        'ruolo' => 'direttore',
                        'livello' => '3',
                        
                        );
        $this->_adminModel->updateDirettore($data, $id);
        $this->_helper->redirector('personale');
        }
		public function cancelladirettoreAction(){
		
		$id= $this->_getParam('direttore');
        $data = array(  
        				
                        'ruolo' => 'clinico',
                        'livello' => '2',
                        
                        );
        $this->_adminModel->updateDirettore($data, $id);
        $this->_helper->redirector('personale');
        }
	
		public function filAction(){
		if(!$this->getRequest()->isPost()) {
        $this->_helper->redirector('analisi');
        }
        $form=$this->_formfiltra;
        if (!$form->isValid($_POST)) {
            return $this->render('analisi');
        }
		
        $values = $form->getValues();
		$var=$values['pazienti'];
		$datapre = $values['datapre'].' '.$values['orapre'].':'.$values['minutipre'].':'.$values['secondipre'];
        $datapost =$values['datapost'].' '.$values['orapost'].':'.$values['minutipost'].':'.$values['secondipost'];
        if($datapre < $datapost){
        $nrterapi=$this->_adminModel->filtrapaziente($datapre, $datapost, $var);
		$nrterapie=count($nrterapi);
		
		
		$parametri= array(
					'nrterapie'=>$nrterapie,
					'da' =>$datapre,
			   		'a' =>$datapost, 
    				);

        $this->_helper->redirector('analisi','admin', TRUE,$parametri);
		}else{
			$form->setDescription('Attenzione! Scegli le date accuratamente');
			return $this->render('analisi');	
		}
	}
	
		private function getfiltraform(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formfiltra = new Application_Form_Admin_Analisi_Filtra();
	    $this->_formfiltra->setAction($urlHelper->url(array(
                'controller' => 'admin',
                'action' => 'fil'),
                'default'
        ));
       return $this->_formfiltra;
	}
		private function getfiltraform2(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_filtraform2 = new Application_Form_Admin_Analisi_Filtra2();
	    $this->_filtraform2->setAction($urlHelper->url(array(
                'controller' => 'admin',
                'action' => 'fil2'),
                'default'
        ));
       return $this->_filtraform2;
	}
		public function fil2Action(){
		if(!$this->getRequest()->isPost()) {
        $this->_helper->redirector('analisi');
        }
        $form=$this->_filtraform2;
        if (!$form->isValid($_POST)) {
            return $this->render('analisi');
        }
		
        $values = $form->getValues();
		$disturbo=$values['disturbo'];		
		$intensita=$values['intensita'];
		$datapre = $values['datapre2'].' '.$values['orapre2'].':'.$values['minutipre2'].':'.$values['secondipre2'];
        $datapost =$values['datapost2'].' '.$values['orapost2'].':'.$values['minutipost2'].':'.$values['secondipost2'];
        
		
		if($datapre < $datapost){
			if($disturbo==0 && $intensita==0)	{
        		$query3=$this->_adminModel->getNrEventiFiltroq1($datapre, $datapost);
			}
			if($disturbo==0 && $intensita!=0)	{
				
	        	$query3=$this->_adminModel->getNrEventiFiltroq2($datapre, $datapost, $intensita);
			}
			if($disturbo!=0 && $intensita==0)	{
				
	       		$query3=$this->_adminModel->getNrEventiFiltroq3($datapre, $datapost, $disturbo);
			}
			if($disturbo!=0 && $intensita!=0)	{
				
	     	   $query3=$this->_adminModel->getNrEventiFiltroq4($datapre, $datapost, $intensita, $disturbo);
			}		
			$query3=count($query3);
				
			$parametri2= array(
						'query3'=>$query3,
						'da2' =>$datapre,
				   		'a2' =>$datapost, 
	    				);
	
	        $this->_helper->redirector('analisi','admin', TRUE,$parametri2);
		}
        else{
			$form->setDescription('Attenzione! Scegli le date accuratamente');
			return $this->render('analisi');	
		}
	}
/****************************************gestione pazienti***********************/
public function newpazientiAction(){
		
	}
	
	public function pazientiAction(){
		$test=$this->_adminModel->getPazienti();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	
	public function cancellapazienteAction(){			
		$riga = $this->_getParam('paziente');
		$this->_adminModel->cancellaPaziente($riga);
		$this->_helper->redirector('pazienti');
	}
	public function modificapazienteAction(){}
	
	private function getUpdatepazientiForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formpazienti = new Application_Form_Admin_Paziente_Modificapaziente;
	    $this->_formpazienti->setAction($urlHelper->url(array(
                'controller' => 'admin',
                'action' => 'updatepaziente',),
                'default'
        ));
       return $this->_formpazienti;
	}
	
	public function updatepazienteAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('pazienti');
        }
        $form=$this->_formpazienti;
        if (!$form->isValid($_POST)) {
            return $this->render('modificapaziente');
        }
		$var= $this->_getParam('paziente');
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                        'cognome' =>$values['cognome'],
                        'eta' =>$values['eta'],
                        'genere' =>$values['genere'],
                        'indirizzo' =>$values['cognome'],
                        'telefono' =>$values['indirizzo'],
                        'email' =>$values['email']     
                                            );
        $this->_adminModel->updatePaziente($data, $var);
        $this->_helper->redirector('pazienti');
	}
	
}
	 