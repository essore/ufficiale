<?php

class PublicController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_clinicaModel;
	protected $_form;
	protected $_authService;	
	
    public function init()
    {
		$this->_helper->layout->setLayout('main');
		$this->_logger = Zend_Registry::get('log');  	
		$this->_clinicaModel= new Application_Model_Guest();
        $this->view->loginForm = $this->getLoginForm();
        $this->_authService = new Application_Service_Auth();
    }

		// Debug
		//$this->_logger->info('Attivato ' . __METHOD__ . ' - Valore di $topCats[0]->name = ' . $topCats[$topId-1]->name);

    public function indexAction()
    {
    	
    }

   
    public function faqAction(){
    	
		$test=$this->_clinicaModel->getFaq();
    	
			$this->view->assign(array(
    		'riga' => $test,
    		)
		);
		
		}
		
    public function viewstaticAction () {
		$page = $this->_getParam('staticPage');
		$this->render($page);
    }	
    
   /*************************Login*****************************/ 
    public function loginAction(){
    }
	
    public function authenticateAction()
	{        
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->_helper->redirector('index');
        }
        $form = $this->_form;
        if (!$form->isValid($request->getPost())) {
            $form->setDescription('Attenzione: alcuni dati inseriti sono errati.');
        	return $this->render('index');
        }
        if (false === $this->_authService->authenticate($form->getValues())) {
            $form->setDescription('Autenticazione fallita. Riprova');
            return $this->render('index');
        }
        
        return $this->_helper->redirector('index', $this->_authService->getIdentity()->ruolo);
	 
	}
	
    

	private function getLoginForm()
    {
    	$urlHelper = $this->_helper->getHelper('url');
		$this->_form = new Application_Form_Public_Auth_Login();
		$this->_form->setAction($urlHelper->url(array(
			'controller' => 'public',
			'action' => 'authenticate'),
			'default'
		));
		return $this->_form;
    }   	
 		public function validateloginAction() 
    {
        $this->_helper->getHelper('layout')->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();

        $loginform = new Application_Form_Public_Auth_Login();
        $response = $loginform->processAjax($_POST); 
        if ($response !== null) {
        	$this->getResponse()->setHeader('Content-type','application/json')->setBody($response);   
        }
    }
}


