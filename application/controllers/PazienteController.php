    <?php 
    class PazienteController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_pazienteModel;
	protected $_form;
	protected $_authService;
	protected $_formpass;
	protected $_formep;
	protected $_formnewep;
	protected $_formfiltra;
	
    public function init()
    {
		$this->_helper->layout->setLayout('paziente');
		$this->_logger = Zend_Registry::get('log');
		$this -> _pazienteModel = new Application_Model_Paziente();	
		$this->view->episodioForm = $this->getEpisodioForm(); 
        $this->_authService = new Application_Service_Auth();
		$this->view->newepisodioForm = $this->getEpisodioForm(); 
		$this->view->modepisodio = $this->getEpisodioUpForm();	
		$this->view->updatepass = $this->getUpdatepassForm();
		$this->view->newprofForm = $this->getProfiloPazienteForm();
		$this->view->filtraform=$this->getfiltraform();
    }
    
	public function indexAction(){
		$mioid =$this->_authService->getIdentity()->idutente;
		$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
		$this->view->assign(array(
							'terapia' => $terapia,
							));
	}
	

	
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
		
    }
	
	public function profiloAction() {
		$mioid =$this->_authService->getIdentity()->idutente;
		$profilo = $this->_pazienteModel->getPaziente($mioid);
		$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
		$this -> view -> assign (array(
									'profilo' => $profilo,
									'terapia' => $terapia, // passo per la notifica
									 )); 
	}
	
	public function newepisodioAction(){
		$mioid =$this->_authService->getIdentity()->idutente;
		$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
		$this->view->assign(array(
							'terapia' => $terapia,
							));
		
	}
	
	public function episodioAction(){
		$mioid=$this->_authService->getIdentity()->idutente;
		if($this->_getParam('filtra',FALSE)){
			
			if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('episodio');
			}
		
			$form=$this->_filtraform;
        	if (!$form->isValid($_POST)) {
            	return $this->render('episodio');
        	}
		
        	$values = $form->getValues();
        	$datapre = $values['datapre'].' '.$values['orapre'].':'.$values['minutipre'].':'.$values['secondipre'];
        	$datapost =$values['datapost'].' '.$values['orapost'].':'.$values['minutipost'].':'.$values['secondipost'];
        	$test = $this->_pazienteModel->filtrapaziente($datapre, $datapost, $mioid);

			$this->view->assign(array(
					'da' =>$datapre,
			   		'a' =>$datapost, 
    				));
		}
		
		else{
			$test=$this->_pazienteModel->getEpisodio($mioid);
		}//estrae tutti gli episodi
		$app=array();//array vuoto
		foreach($test as $x){					//scorro tutti gli episodi
			$num=$x->disturbo;					//estrai l'id del disturdo presente nella tabella episodi
			$var=$this->_pazienteModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
			$app+=array($num=>$var->nome);		//metto in coda all'array associativo feature id valore nome disturbo
		}
		$terapia = $this->_pazienteModel->getTerapia($mioid); // passo per la notifica
		$alert = $this->getParam('script');
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'riga' => $test,
			   		'dist'=>$app,
			   		'terapia' => $terapia,
			   		'alert' => $alert, //passo per la notifica
    				));
		}


	private function getfiltraform(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_filtraform = new Application_Form_Paziente_Episodio_Filtra();
	    $this->_filtraform->setAction($urlHelper->url(array(
                'controller' => 'paziente',
                'action' => 'episodio',
				'filtra' => TRUE,
				),
                'default'
        ));
       return $this->_filtraform;
	}

	
	public function addepisodioAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('episodio');
			}
		$form=$this->_formnewep;
		if (!$form->isValid($_POST)) {
			return $this->render('newepisodio');
		}
				
		$values = $form->getValues();
		$mioid =$this->_authService->getIdentity()->idutente;
		$data = array(   
         				'paziente' => $mioid,
                        'disturbo' =>$values['disturbo'],
                        'intensita' =>$values['intensita'],
                        'durata' =>$values['durata'],
                        'data' =>$values['data'].' '.$values['ora'].':'.$values['minuti'].':'.$values['secondi'],
                       
                        );
		$this->_pazienteModel->saveEpisodio($data);
		$script = array('script' => '1');
		$this->_helper->redirector('episodio','paziente',true,$script);
 	}	   

public function modepisodioAction(){
	$mioid =$this->_authService->getIdentity()->idutente;
	$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
	$this->view->assign(array(
						'terapia' => $terapia,
						));
}

private function getEpisodioUpForm(){
	
       $urlHelper = $this->_helper->getHelper('url');
       $this->_formep = new Application_Form_Paziente_Episodio_Modepisodio();
	   $idep = $this->_getParam('id');
	   $this->_formep->setAction($urlHelper->url(array(
                'controller' => 'paziente',
                'action' => 'updateepisodio',
				'id' => $idep
				),
                'default'
        ));
       return $this->_formep;
   }


public function updateepisodioAction()
    {
        if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('updateepisodio');
        }
        $form=$this->_formep;
        if (!$form->isValid($_POST)) {
            return $this->render('newepisodio');
        }
		//$var= $this->_authService->getIdentity()->idutente;
        $values = $form->getValues();
		$idep = $this->_getParam('id');
        $data = array(   
         				
                        'disturbo' =>$values['disturbo'],
                        'intensita' =>$values['intensita'],
                        'durata' =>$values['durata'],
                        'data' =>$values['data'].' '.$values['ora'].':'.$values['minuti'].':'.$values['secondi'],
                       
                        );

	    $this->_pazienteModel->updateEpisodio($data, $idep);
        $this->_helper->redirector('episodio');
    }

		
	private function getEpisodioForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_formnewep = new Application_Form_Paziente_Episodio_Add();
			$this->_formnewep->setAction($urlHelper->url(array(
					'controller' => 'paziente',
					'action' => 'addEpisodio'),
					'default',
					true
					));
			return $this->_formnewep;
	}   
	public function cancellaepisodioAction(){			
		$riga = $this->_getParam('id');
		$this->_pazienteModel->cancellaEpisodio($riga);
		$this->_helper->redirector('episodio');
	}
		
	public function cancellallAction(){
		$mioid= $this->_authService->getIdentity()->idutente;
		$this->_pazienteModel->cancellatuttiEpisodi($mioid);
		$this->_helper->redirector('episodio');
	}	
		
	public function cancellallfiltratiAction(){
		$mioid= $this->_authService->getIdentity()->idutente;
		$da = $this->_getParam('da');
		$a = $this->_getParam('a');
		$this->_pazienteModel->cancellaepisodifiltrati($mioid, $da, $a);
		$this->_helper->redirector('episodio');
	}	
		
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}
	
  	public function faqAction(){
  		$mioid =$this->_authService->getIdentity()->idutente;
    	$terapia = $this->_pazienteModel->getTerapia($mioid);
		$test=$this->_pazienteModel->getFaq();
    	
			$this->view->assign(array(
    		'riga' => $test,
    		'terapia' => $terapia, //passo per la notifica
    		)
		);
		
		}
public function cartellaAction(){ // action che serve per la cartella clinica del paziente.
		$mioid =$this->_authService->getIdentity()->idutente;
		$cartella = $this->_pazienteModel->getPaziente($mioid);
		$this -> view -> assign (array(
									'cartella' => $cartella,
									 ));
									 
		$disturbo = $this->_pazienteModel->getDisturbiDi($mioid);//estrae tutti gli episodi
		$app=array();//array vuoto
		foreach($disturbo as $x){//scorro tutti gli episodi
			$num=$x->iddisturbo;//estrai l'id del disturdo presente nella tabella episodi
			$var=$this->_pazienteModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
			$app+=array($num=>$var->nome);//metto in coda all'array associativo feature id valore nome disturbo
		}
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'riga' => $disturbo,
			   		'dist'=>$app,
    				));
					
		$episodio = $this->_pazienteModel->getEpisodio($mioid);
		if($episodio != null){
			$epi=array();//array vuoto
			foreach($episodio as $x){//scorro tutti gli episodi
				$num=$x->disturbo;//estrai l'id del disturdo presente nella tabella episodi
				$var=$this->_pazienteModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
				$epi+=array($num=>$var->nome);//metto in coda all'array associativo feature id valore nome disturbo	
			}
		}
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'episodio' => $episodio,
			   		'epi'=>$epi,
    				));
		
		$clinico = $this->_pazienteModel->seguitoDa($mioid); // da questa parte ricavo i medici da cui sono seguito
		$cli = array();
		foreach($clinico as $x){
			$id = $x->idclini;
			$var = $this->_pazienteModel->getClinicoById($id);
			$cli+=array($id =>$var->cognome);			
		}
		$this->view->assign(array(
							'clinico' => $clinico,
							'cli' => $cli,
							));
		
		$terapia = $this->_pazienteModel->getTerapia($mioid);// da questa parte ricavo da chi e quando è stata assegnata la terapia
		$this->view->assign(array(
							'terapia' => $terapia,
							));
		
		$att = array();
		$farm = array();
		$idatt = array();
		$idfarm = array();
		$durat = array();
		$dosag = array();
		foreach($terapia as $x){
			$id = $x->idterapia;
			$presca = $this->_pazienteModel->getAttivita($id);
			$prescf = $this->_pazienteModel->getPresFarmaco($id);
			if(count($prescf)!=0){
				$idfarm += array($id => $prescf->idfarm);
				$dosag += array($id => $prescf->dosaggio);
				$farmaco = $this->_pazienteModel->getFarmacoById($prescf->idfarm);
				$farm+=array($id => $farmaco->nome);
			}else{
				$idfarm += array($id => null);
				$dosag += array($id => null);
				$farm+=array($id => null);
			}
			if(count($presca)!=0){
				$idatt += array($id => $presca->idatt);
				$durat += array($id => $presca->durata);
				$attivita = $this->_pazienteModel->getAttivitaById($presca->idatt);
				$att+=array($id => $attivita->nome);
			}else{
				$idatt += array($id => null);
				$durat += array($id => null);
				$att+=array($id => null);
			}
		}
		$this->view->assign(array(
									'att' => $att,
									'durata' => $durat,
									'farm' => $farm,
									'dosaggio' => $dosag,
									));
			
	}

	public function modificapassAction(){}
	
	private function getUpdatepassForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formpass = new Application_Form_Paziente_Updatepass();
	    $this->_formpass->setAction($urlHelper->url(array(
                'controller' => 'paziente',
                'action' => 'updatepass'),
                'default'
        ));
       return $this->_formpass;
	}
	
	public function updatepassAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('profilo');
        }
        $form=$this->_formpass;
        if (!$form->isValid($_POST)) {
            return $this->render('modificapass');
        }
		$var= $this->_authService->getIdentity()->idutente;
        //$id = $this->_clinicoModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
		if(!is_null($this->_pazienteModel->getUtenteByName($values['username']))){
			$form->setDescription('username già occupato');
			return $this->render('modificapass');	
		}
        $data = array(    
                        'username' =>$values['username'],
                        'password' =>$values['password'],
                       
                        );
        $this->_pazienteModel->updateCredenziali($data, $var);
        $this->_helper->redirector('profilo');
	}
	
	private function getProfiloPazienteForm(){
       $urlHelper = $this->_helper->getHelper('url');
       $this->_form = new Application_Form_Paziente_Modprof();
        $this->_form->setAction($urlHelper->url(array(
                'controller' => 'paziente',
                'action' => 'updatepaziente'),
                'default'
        ));
       return $this->_form;
   }
	public function updatepazienteAction()
    {
        if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('profilo');
        }
        $form=$this->_form;
        if (!$form->isValid($_POST)) {
            return $this->render('index');
        }
		$var= $this->_authService->getIdentity()->idutente;
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                        'cognome' =>$values['cognome'],
                        'eta' =>$values['eta'],
                        'genere' =>$values['genere'],
                        'indirizzo' =>$values['cognome'],
                        'telefono' =>$values['indirizzo'],
                        'email' =>$values['email']                       
                        );
        $this->_pazienteModel->updatePaziente($data, $var);
        $this->_helper->redirector('profilo');
    }
	public function newprofAction(){
    }
	public function validateepAction() 
    {
        $this->_helper->getHelper('layout')->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();

        $newepisodioForm = new Application_Form_Paziente_Episodio_Add();
        $response = $newepisodioForm->processAjax($_POST); 
        if ($response !== null) {
        	$this->getResponse()->setHeader('Content-type','application/json')->setBody($response);        	
        }
    }


	public function whereAction(){
		$mioid =$this->_authService->getIdentity()->idutente;
		$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
		$this->view->assign(array(
							'terapia' => $terapia,
							));
	}
	
	public function whoAction(){
		$mioid =$this->_authService->getIdentity()->idutente;
		$terapia = $this->_pazienteModel->getTerapia($mioid);// passo per la notifica
		$this->view->assign(array(
							'terapia' => $terapia,
							));
	}
	
	public function notificaAction(){
		$mioid =$this->_authService->getIdentity()->idutente;
		$this->_helper->getHelper('layout')->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
		$this->_pazienteModel->setNotifica($mioid);
		$this->_helper->redirector('cartella');
	}
	
}

