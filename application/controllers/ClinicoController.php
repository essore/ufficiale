<?php 
    class ClinicoController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_clinicoModel;
	protected $_formterapia;
	protected $_authService;
	protected $_formpaz;
	protected $_form;
	protected $_searchform;
	protected $_formprof;
	protected $_filtraform;
	

    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');
		$this->_clinicoModel= new Application_Model_Clinico();
		$this->_authService = new Application_Service_Auth();
		$this->view->addpazienteform = $this->getpazForm(); /*link tra vista e form*/
		$this->view->newprofForm = $this->getProfiloClinicoForm();
		$this->view->addterapiaForm = $this->newTerapiaForm();
		$this->view->updatepass = $this->getUpdatepassForm();
		$this->view->search = $this->getSearchForm();
		$this->view->alldisturbi = $this->getAllDisturbiForm();	
		$this->view->filtraform=$this->getfiltraform();
    }
    
	public function indexAction(){}
	
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
	   	
		
	/*******************************estrare tutti i clinici********************/	
	public function clinicoAction(){  //la vista di tutti i clinici
	$test=$this->_clinicoModel->getClinico();
	$this->view->assign(array(
	   				'riga' => $test,
					));
	}
	
	public function clinicobyidAction(){//la vista di un clinico
		$id = $this->_getparam('id',null);

		if(!is_null($id)){
			$test=$this->_clinicoModel->getClinicoById($id);
		}else{
			$id = $this->_authService->getIdentity()->idutente;	
			$test=$this->_clinicoModel->getClinicoById($id); //da inserire l'id dell'utente loggato
		}
		$this->view->assign(array(
		   				'clinico' => $test,
					));
	}
	
	public function viewpazienteAction(){//la vista di un paziente
		$id = $this->_getparam('id',null);
		$mio =  $this->_getparam('mio',null);
		
		$test=$this->_clinicoModel->getPazienteById($id);
		$this->view->assign(array(
		   				'paziente' => $test,
		   				'mio'=> $mio
					));
	}
	
	    public function viewallpazientiAction(){
    	
		$id=$this->_authService->getIdentity()->idutente;
		$segue=$this->_clinicoModel->mypaziente($id);
		$bla=array(''=>'');
		$i=0;
		foreach($segue as $y){
				$bla += array($i++=>$y->idpazi);
		//	echo $bla[$i++];
		}
    	
    	$notmieipazienti = $this->_clinicoModel->notmypaziente($bla);
    	$nome= array();
		$cognome= array();
		$eta= array();
    	
    	foreach($notmieipazienti as $x){
    		$pippo = $this->_clinicoModel->getPazienteById($x->idpaziente);
			$nome += array($x->idpaziente => $pippo->nome);
			$cognome += array($x->idpaziente => $pippo->cognome);
			$eta += array($x->idpaziente => $pippo->eta);
		}    	
			$this->view->assign(array(
			'segue'=>$notmieipazienti,
    		'nome' => $nome,
    		'cognome'=> $cognome,
    		'eta'=> $eta
    		)
		);
		
		
		}
	
	
	
	
	private function getSearchForm(){
		$urlHelper = $this->_helper->getHelper('url');
		$this->_searchform = new Application_Form_Clinico_Paziente_Search();
		$this->_searchform->setAction($urlHelper->url(array(
					'controller' => 'clinico',
					'action' => 'mypazienti',
					'search' => 1), /*****azione legata alla submit della form*/
					'default',
					true
					));
		return $this->_searchform;	
		
	}
	
/**************************************************************miei pazienti*/	
	public function mypazientiAction(){
		$id=$this->_authService->getIdentity()->idutente;
		
		$segue=$this->_clinicoModel->mypaziente($id);

		//estrapolo l'id di tutti i miei pazienti
		$idpazienti=array(''=>'');
	    foreach($segue as $x)$idpazienti+=array($x->idpazi=>$x->idpazi);  
		
		if($this->_getParam('search',FALSE)){  //se la form del filtro è piena 
		
			if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('viewallpazienti');
			}
			$form = $this->_searchform;
			if (!$form->isValid($_POST)) {
				return $this->render('viewallpazienti');
			}
			$values = $form->getValues();
			$where=NULL;
			
			$nome=$values['nome'];
			
				//se l'ultimo carattere è *
				if(substr($nome, -1)=='*')$pazienti = $this->_clinicoModel->cercapazienti($idpazienti, rtrim($nome, "*"),1);//recuperto tutti i dati dalla tabella pazienti in base agli id ricavati sopra
				//altrimenti ricerco query con =	
				else $pazienti = $this->_clinicoModel->cercapazienti($idpazienti, $nome, 0);
			}
	else{
		$pazienti = $this->_clinicoModel->recuperapazienti($idpazienti);
	}

	$this->view->assign(array(
			'pazienti'=>$pazienti
    		)
		);
		
	}
	
	public function addpazienteAction() {
		
	}
	private function getpazForm(){
			$urlHelper = $this->_helper->getHelper('url');
			$this->_formpaz = new Application_Form_Clinico_Paziente_Add();
			$this->_formpaz->setAction($urlHelper->url(array(
					'controller' => 'clinico',
					'action' => 'newpaziente'), /*****azione legata alla submit della form*/
					'default',
					true
					));
			return $this->_formpaz;	
	}
	
	public function newpazienteAction(){
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('addpaziente');
			}
		$formpaz=$this->_formpaz;
		if (!$formpaz->isValid($_POST)) {
			return $this->render('addpaziente');
		}
		//con una sola form vogliamo fare l'insert in 2 tabelle distinte, attraverso gli array associativi
		//possiamo separarei i dati e fare l'insert prima nella tabella user poi nella tabella pazienti.
		
		$values = $formpaz->getValues(); //recupero dati dalla form
		
		
		if(!is_null($this->_clinicoModel->getUtenteByName($values['username']))){ //controllo se l'username richiesto è già presente
			$formpaz->setDescription('username già occupato');
			return $this->render('addpaziente');	
		}

		$user = array(	'username' =>$values['username'],
						'password' =>$values['password'],
						'ruolo'  =>'paziente'
						);
		$id= $this->_clinicoModel->newutente($user); //insert nella tabella utenti e ritorna 
		$paziente = array(
						'idpaziente' => $id,	
						'nome' =>$values['nome'],
						'cognome' =>$values['cognome'],
						'eta' =>$values['eta'],
						'genere' =>$values['genere'],
						'indirizzo' =>$values['indirizzo'],
						'telefono' =>$values['telefono'],
						'email' =>$values['email'],
						);
		
		$this->_clinicoModel->newpaziente($paziente);
		if($values['associa']){
			
			$info=array('idpazi'=>$id ,
						'idclini'=>$this->_authService->getIdentity()->idutente);
 			$this->_clinicoModel->newsegue($info);
		
		}
		
		
		$parametri = array('id'=> $id);
		$this->_helper->redirector('viewpaziente','clinico',TRUE, $parametri); 
		 /* rimanda alla visualizzazione del paziente*/
		/*'clinicobyid/id/'.$id -> stampa: http://localhost/ritenta/public/index.php/clinico/clinicobyid%2Fid%2F35*/
	}


		public function seguiloAction(){
		$idp = $this->_getparam('id',null);	
		$idc = $this->_authService->getIdentity()->idutente;
		
		if( !$this->_clinicoModel->esistesegue($idc, $idp)){
			$parameters=array('id'=>$idp,'mio'=>1);
			$this->_helper->redirector('viewpaziente','clinico',TRUE,$parameters); 
		}		
		$info=array('idpazi'=>$idp ,
						'idclini'=>$idc);
 		$this->_clinicoModel->newsegue($info);
		$this->_helper->redirector('mypazienti','clinico',TRUE); 
}
	
/****cucx********************************************/
	public function newprofAction(){
        
    }	
	private function getProfiloClinicoForm(){
       $urlHelper = $this->_helper->getHelper('url');
       $this->_formprof = new Application_Form_Clinico_Modprof();
        $this->_formprof->setAction($urlHelper->url(array(
                'controller' => 'clinico',
                'action' => 'updateclinico'),
                'default'
        ));
       return $this->_formprof;
   }
	
	public function updateclinicoAction()
    {
        if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('clinicobyid');
        }
        $form=$this->_formprof;
        if (!$form->isValid($_POST)) {
            return $this->render('newprof');
        }
		$var= $this->_authService->getIdentity()->idutente;
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                        'cognome' =>$values['cognome'],
                        'ruolo' =>$values['ruolo'],
                        'specializzazione' =>$values['specializzazione'],
                        );
        $this->_clinicoModel->updateClinico($data, $var);
        $this->_helper->redirector('clinicobyid');
    }
	public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}

  	public function faqAction(){
    	
		$test=$this->_clinicoModel->getFaq();
    	
			$this->view->assign(array(
    		'riga' => $test,
    		));
	}  	
/********************************************************************************************************gestione disturbi del paziente*******/	 	
	
	public function disturboAction(){
		$idpaziente=$this->getParam('id');
		$paziente=$this->_clinicoModel->getPazienteById($idpaziente);
		$this->view->assign(array('paziente'=>$paziente));
		//recupera dati dei disturbi già presenti
		$patologie= $this->_clinicoModel->getDisturbiDi($idpaziente);
		if(count($patologie)>0){
			$dist=array();
			foreach($patologie as $p)$dist+=array($p->iddisturbo=>$p->iddisturbo);
			$disturbi=$this->_clinicoModel->recuperaDisturbi($dist);
			$this->view->assign(array('disturbi'=>$disturbi));	
		}
	}
	private function getAllDisturbiForm(){
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Clinico_Paziente_Disturbi();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'clinico',
					'action' => 'assegnadisturbo',
					'id' => $this->getParam('id')), 
					'default',
					true
					));
			return $this->_form;	
	}
	public function assegnadisturboAction()
	{
		$idpaziente=$this->getParam('id');
		$parametri=array('id'=>$idpaziente);  

        if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('disturbo');
        }
        $form=$this->_form;
        if (!$form->isValid($_POST)) {
            return $this->render('disturbo');
        }
        $values = $form->getValues();	
			
		$esiste=$this->_clinicoModel->esistePatologia($values['disturbo'], $idpaziente); //ritorna un boolean!
		if(!$esiste){
			$info=array('iddisturbo'=>$values['disturbo'],'idpaziente'=>$idpaziente);
			$this->_clinicoModel->setpatologia($info);
		}
		$this->_helper->redirector('disturbo','clinico',TRUE,$parametri);
		
	}
	public function cancellapatologiaAction()
	{
		$idpaziente=$this->getParam('paziente');
		$iddisturbo=$this->getParam('disturbo');
		$this->_logger->info('paziente'.$idpaziente.' disturbo: '.$iddisturbo);
		
		$where=array('idpaziente = '.$idpaziente.' AND iddisturbo = '.$iddisturbo);
		$this->_clinicoModel->cancellaPatologia($where);
		$parametri=array('id'=>$idpaziente); 
		$this->_helper->redirector('disturbo','clinico',TRUE,$parametri);
	}  	

	public function modificapassAction(){}

	public function addterapiaAction(){
	}	
	
	private function newTerapiaForm(){
		$urlHelper = $this->_helper->getHelper('url');
			$this->_formterapia = new Application_Form_Clinico_Terapia_Add();
			$this->_formterapia->setAction($urlHelper->url(array(
					'controller' => 'clinico',
					'action' => 'terapia',
					'id' => $this->getParam('id')), //azione legata alla submit della form
					'default',
					true
					));
			return $this->_formterapia;	
	}
	
	public function terapiaAction(){
		$id=$this->_authService->getIdentity()->idutente;
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('addterapia');
			}
		$formterapia=$this->_formterapia;
		if (!$formterapia->isValid($_POST)) {
			return $this->render('addterapia');
		}
		$values = $formterapia->getValues(); //recupero dati dalla form
		$clinico = $this->_clinicoModel->getClinicoById($id);
		$idpaziente = $this->getParam('id');

		$terapia = array(	'notifica' =>'1',
							'clinico' => $clinico->cognome,
							'idpaziente' => $idpaziente,
							'data'  =>new Zend_Db_Expr('NOW()'), // da controllare l'esattezza della creazione dell'oggetto
						);
		if($terapia['notifica']!= null && $terapia['clinico']!= null && $terapia['idpaziente']!= null && $terapia['data']!= null){
			$this->_clinicoModel->addTerapia($terapia); //insert nella tabella utenti
		}
		else {
			return $this->render('index');
		}
		
		$result = $this->_clinicoModel->getTerapiaId($clinico->cognome);
		$idTerapia = $result->idterapia;
		if($values['attivita']!== null && $values['durata']!== null){ // posso sostituire il controllo con il ruolo
			$prescrizione = array(
						'idter' => $idTerapia,	
						'durata' =>$values['durata'],
						'idatt' =>$values['attivita'],
						);
			$this->_clinicoModel->prescriviAttivita($prescrizione);
		}else{
			$prescrizione = array(
						'idter' => $idTerapia,	
						'dosaggio' =>$values['dosaggio'],
						'idfarm' =>$values['farmaco'],
						);
			$this->_clinicoModel->prescriviFarmaco($prescrizione);
		}
		$parametri = array('id'=> $idpaziente, 'mio'=>1);
		$this->_helper->redirector('viewpaziente','clinico',TRUE,$parametri);  
	}

	
	private function getUpdatepassForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formpass = new Application_Form_Clinico_Updatepass();
	    $this->_formpass->setAction($urlHelper->url(array(
                'controller' => 'clinico',
                'action' => 'updatepass'),
                'default'
        ));
       return $this->_formpass;
	}
	
	public function updatepassAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('clinicobyid');
        }
        $form=$this->_formpass;
        if (!$form->isValid($_POST)) {
            return $this->render('modificapass');
        }
		$var= $this->_authService->getIdentity()->idutente;
        //$id = $this->_clinicoModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
        $data = array(    
                        'username' =>$values['username'],
                        'password' =>$values['password'],
                       
                        );
        $this->_clinicoModel->updateCredenziali($data, $var);
        $this->_helper->redirector('clinicobyid');
	}
	
	private function getfiltraform(){
		$id=$this->_getParam('id');
		$urlHelper = $this->_helper->getHelper('url');
        $this->_filtraform = new Application_Form_Clinico_Paziente_Filtra();
	    $this->_filtraform->setAction($urlHelper->url(array(
                'controller' => 'clinico',
                'action' => 'episodio',
				'filtra' => TRUE,
				'id'=> $id,
				),
                'default'
        ));
       return $this->_filtraform;
	}


	public function episodioAction(){
		
		$id=$this->_getParam('id');
		if($this->_getParam('filtra',FALSE)){
			
			if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('episodio');
			}
		
			$form=$this->_filtraform;
        	if (!$form->isValid($_POST)) {
            	return $this->render('episodio');
        	}
			$values = $form->getValues();
        	$disturbo=$values['disturbo'];		
			$intensita=$values['intensita'];
			$durata=$values['durata'];
        
        	$where="";	
			
			//ANALISI DEL FILTRO
			
			if($values['disturbo']!='')	$where = 'disturbo = \''.$disturbo.'\'';
			
			if($values['intensita']!= 0){
				if($where != NULL)$where = $where.' AND ';
				$where = $where.'intensita = \''.$values['intensita'].'\'';	
			}
			
			if($values['durata']!= ''){
				if($where != NULL)$where = $where.' AND ';
				$where = $where.'durata = \''.$values['durata'].'\'';	
			}
		     
			if($values['datapre']!= '' OR $values['datapost']!= ''){
					
				$datapre = $values['datapre'].' '.$values['orapre'].':'.$values['minutipre'].':'.$values['secondipre'];
        		$datapost =$values['datapost'].' '.$values['orapost'].':'.$values['minutipost'].':'.$values['secondipost'];
 
				if($datapre < $datapost){	
					if($where != NULL)$where = $where.' AND ';
					$where = $where.'data > \''.$datapre.'\' AND data < \''.$datapost.'\'';	
				}else{
				$form->setDescription('Attenzione inserisci un intervallo di date valide');
				return $this->render('episodio');        
				}
			}	
			$test = $this->_clinicoModel->filtrapa($id, $where);
		}
		else{
			$test=$this->_clinicoModel->getEpisodio($id);
		}
		
		
		$app=array();//array vuoto
		
		foreach($test as $x){					//scorro tutti gli episodi
			$num = $x->disturbo;					//estrai l'id del disturdo presente nella tabella episodi
			$var = $this->_clinicoModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
			$app+=array($num=>$var->nome);		//metto in coda all'array associativo feature id valore nome disturbo
		}
		$terapia = $this->_clinicoModel->getTerapia($id); // passo per la notifica
		$alert = $this->getParam('script');
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'riga' => $test,
			   		'dist'=>$app,
			   		'terapia' => $terapia,
			   		'alert' => $alert, //passo per la notifica
			   		'utente'=>$id,
	    				));
		}

public function cartellaAction(){
	
		$idp= $this->_getParam('id'); 
		$cartella = $this->_clinicoModel->getPazienteById($idp);
		$this -> view -> assign (array(
									'cartella' => $cartella,
									 ));
									 
		$disturbo = $this->_clinicoModel->getDisturbiDi($idp);//estrae tutti gli episodi
		$app=array();//array vuoto
		foreach($disturbo as $x){//scorro tutti gli episodi
			$num=$x->iddisturbo;//estrai l'id del disturdo presente nella tabella episodi
			$var=$this->_clinicoModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
			$app+=array($num=>$var->nome);//metto in coda all'array associativo feature id valore nome disturbo
		}
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'riga' => $disturbo,
			   		'dist'=>$app,
    				));
					
		$episodio = $this->_clinicoModel->getEpisodio($idp);
		if($episodio != null){
			$epi=array();//array vuoto
			foreach($episodio as $x){//scorro tutti gli episodi
				$num=$x->disturbo;//estrai l'id del disturdo presente nella tabella episodi
				$var=$this->_clinicoModel->getdisturbobyid($num);//estraggo solo il disturbo attraverso l'id
				$epi+=array($num=>$var->nome);//metto in coda all'array associativo feature id valore nome disturbo	
			}
		}
		$this->view->assign(array(//mando alla vista due info una sugli episodi e uno con il nome dei disturbi
			   		'episodio' => $episodio,
			   		'epi'=>$epi,
    				));
		
		$clinico = $this->_clinicoModel->seguitoDa($idp); // da questa parte ricavo i medici da cui sono seguito
		$cli = array();
		foreach($clinico as $x){
			$id = $x->idclini;
			$var = $this->_clinicoModel->getClinicoById($id);
			$cli+=array($id =>$var->cognome);			
		}
		$this->view->assign(array(
							'clinico' => $clinico,
							'cli' => $cli,
							));
		
		$terapia = $this->_clinicoModel->getTerapia($id);// da questa parte ricavo da chi e quando è stata assegnata la terapia
		$this->view->assign(array(
							'terapia' => $terapia,
							));
		
		$att = array();
		$farm = array();
		$idatt = array();
		$idfarm = array();
		$durat = array();
		$dosag = array();
		foreach($terapia as $x){
			$id = $x->idterapia;
			$presca = $this->_clinicoModel->getAttivita($id);
			$prescf = $this->_clinicoModel->getPresFarmaco($id);
			if(count($prescf)!=0){
				$idfarm += array($id => $prescf->idfarm);
				$dosag += array($id => $prescf->dosaggio);
				$farmaco = $this->_clinicoModel->getFarmacoById($prescf->idfarm);
				$farm+=array($id => $farmaco->nome);
			}else{
				$idfarm += array($id => null);
				$dosag += array($id => null);
				$farm+=array($id => null);
			}
			if(count($presca)!=0){
				$idatt += array($id => $presca->idatt);
				$durat += array($id => $presca->durata);
				$attivita = $this->_clinicoModel->getAttivitaById($presca->idatt);
				$att += array($id => $attivita->nome);
			}else{
				$idatt += array($id => null);
				$durat += array($id => null);
				$att += array($id => null);
			}
		}
		$this->view->assign(array(
									'att' => $att,
									'durata' => $durat,
									'farm' => $farm,
									'dosaggio' => $dosag,
									));
			
	}
}
	
