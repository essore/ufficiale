    <?php 
    class DirettoreController extends Zend_Controller_Action
{
	protected $_logger;
	protected $_direttoreModel;
	
	protected $_form;
	protected $_filtraform;
	protected $_searchform;
	protected $_assegnaclinicoform;

	protected $_authService;
	
    public function init()
    {
		$this->_helper->layout->setLayout('clinico');
		$this->_logger = Zend_Registry::get('log');  
		$this->_direttoreModel= new Application_Model_Direttore();	
			
		$this->view->disturboForm = $this->getDisturboForm();
		$this->view->attivitaForm = $this->getAttivitaForm();
		$this->view->farmacoForm = $this->getFarmacoForm();
		$this->view->filtrapazienti = $this->getfiltraForm();
		$this->view->updatedisturbo = $this->getUpdatedisturboForm();
		$this->view->updatefarmaco = $this->getUpdatefarmacoForm();
		$this->view->updateattivita = $this->getUpdateattivitaForm();
		$this->view->search = $this->getSearchForm();
		$this->view->assegnaform = $this->getassegnaclinicoForm();
		
        $this->_authService = new Application_Service_Auth();		
    }
	
	public function indexAction(){}
    
    public function viewstaticAction () {
    	$page = $this->_getParam('staticPage');
    	$this->render($page);
    }
/***************************************************************************lista pazienti*****/	
	public function pazientiAction()
	{
		$id=$this->_authService->getIdentity()->idutente;
		
		$pazienti=$this->_direttoreModel->getpazienti();//recupero i dati della tabella pazienti
		
		//estrapolo l'id di tutti i pazienti
		$idpazienti=array(''=>'');
	    foreach($pazienti as $x) $idpazienti+=array($x->idpaziente=>$x->idpaziente);  
		
		if($this->_getParam('search',FALSE)){  //se la form del filtro è piena 
		
			if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('pazienti');
			}
			$form = $this->_searchform;
			if (!$form->isValid($_POST)) {
				return $this->render('pazienti');
			}
			$values = $form->getValues();
			$where=NULL;
			
			$nome=$values['nome'];
			
			//se l'ultimo carattere è *
			if(substr($nome, -1)=='*')$pazienti = $this->_direttoreModel->cercapazienti($idpazienti, rtrim($nome, "*"),1);//recuperto tutti i dati dalla tabella pazienti in base agli id ricavati sopra
			//altrimenti ricerco query con =	
			else $pazienti = $this->_direttoreModel->cercapazienti($idpazienti, $nome, 0);
	}
	else{
		$pazienti = $this->_direttoreModel->recuperapazienti($idpazienti);
	}

	$this->view->assign(array(
			'pazienti'=>$pazienti
    		));
	}

	private function getSearchForm(){
		$urlHelper = $this->_helper->getHelper('url');
		$this->_searchform = new Application_Form_Clinico_Paziente_Search();
		$this->_searchform->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'pazienti',
					'search' => 1), /*****azione legata alla submit della form*/
					'default',
					true
					));
		return $this->_searchform;	
	}
	
	public function viewpazienteAction(){//la vista di un paziente
		$id = $this->_getparam('id',null);
		$this->_logger->info('PUBLIC view pazienti');
		$paziente=$this->_direttoreModel->getPazienteById($id);
		
		$clinico = $this->_direttoreModel->seguitoDa($id); // da questa parte ricavo i medici da cui sono seguito
		$clinome = array();
		$clicog = array();
		foreach($clinico as $x){
			$id = $x->idclini;
			$var = $this->_direttoreModel->getClinicoById($id);
			$clicog+=array($id =>$var->cognome);
			$clinome+=array($id =>$var->nome);			
					
		}
		$this->view->assign(array(
							'paziente' => $paziente,
							'clinico' => $clinico,
							'clinome' => $clinome,
							'clicog' => $clicog,
							'from' =>$this->_getparam('from'),
							));
	}

	public function assegnaclinicoAction()
	{
	}
	
	private function getassegnaclinicoForm()
	{
		$urlHelper = $this->_helper->getHelper('url');
		$this->_logger->info('PRIVATE get assegna clinico form');
		
		$this->_assegnaclinicoform = new Application_Form_Direttore_Segue_Assegnaclinico;
		
		$id = $this->_getparam('id',null);		
		$this->_assegnaclinicoform->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'assegna',
					'id' => $id),
					'default',
					true
					));
		return $this->_assegnaclinicoform;		
	}
	
	public function deleteAction(){ 		/*elimina l'associazione tra clinico e paziente*/
		$idpazi = $this->_getparam('idpazi',null);
		$idclini =  $this->_getparam('idclini',null);
		$where=array('idclini = '.$idclini.' AND idpazi = '.$idpazi);
		
		$this->_direttoreModel->eliminasegue($where);
		
		
		$redirect=$this->_getparam('redirect',null);
		if($redirect=='viewpaziente'){
			$parameters=array('id'=>$idpazi);
			$this->_helper->redirector('viewpaziente','direttore',true,$parameters);
		}	
		if($redirect=='cliniciepazienti'){
			$this->_helper->redirector('cliniciepazienti','direttore');
		}
		$this->_helper->redirector('pazienti','direttore');
	}
	
	
	public function assegnaAction(){		/* l'associa clinico e paziente*/
		$idpaz = $this->_getparam('id',null);		
	
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('pazienti');
			}
		
		$form=$this->_assegnaclinicoform;
		if (!$form->isValid($_POST)) {
			return $this->render('assegnaclinico');
		}
		
		$values = $form->getValues();
		
		$esiste = $this->_direttoreModel->esistesegue($values['clinico'], $idpaz); //controllo se l'insert che voglio fare esiste già!
		if(count($esiste) > 0){
			$form->setDescription('clinico già associato');//se esiste pubblico un messaggio di errore
			$this->render('assegnaclinico'); 
		}
		else{
			$info=array('idpazi' => $idpaz,
						'idclini'=> $values['clinico']);
			$this->_direttoreModel->newsegue($info);
			
			$parameters=array('id'=>$idpaz);
			$this->_helper->redirector('viewpaziente','direttore',true,$parameters);
		}
	}
	
	
	
/************************************************************************pazienti e clinici***/	
	
	
	public function cliniciepazientiAction(){  //la vista di tutti i pazienti per ogni clinico
	
	if($this->_getParam('filtra',FALSE)){  //se la form del filtro è piena 
		
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('cliniciepazienti');
		}
		$form=$this->_filtraform;
		if (!$form->isValid($_POST)) {
			return $this->render('cliniciepazienti');
		}
		$values = $form->getValues();
		$where=NULL;
		
		//ANALISI DEL FILTRO
		
		if($values['eta']!='')	$where = $where.'eta '.$values['range'].' '.$values['eta'];
		
		if($values['genere']!= NULL){
			if($where != NULL)$where = $where.' AND ';
			$where = $where.'genere = \''.$values['genere'].'\'';	
		}
		
		if($where != NULL) $pazienti=$this->_direttoreModel->filtrapazienti($where);	//filtro i pazienti per eta e genere
		else $pazienti=$this->_direttoreModel->getpazienti();							//altrimenti li prendo tutti
			
		
		if($values['disturbo']!='0'){	//se ho specificato un filtro per disturbo						
			
			$idpazienti=array(''=>'');
			foreach($pazienti as $p) $idpazienti += array($p->idpaziente => $p->idpaziente); //recupero l'id di tutti i pazienti filtrati in precedenza
			
			$disturbati = $this->_direttoreModel->filtradisturbi($values['disturbo'],$idpazienti); //filtro i pazienti che hanno quel disturbo
			
			$idpazienti=array(''=>'');
			foreach($disturbati as $p)	$idpazienti += array($p->idpaziente => $p->idpaziente); //recupero l'id dei pazienti con quel disturbo
			
			$pazienti=$this->_direttoreModel->recuperapazienti($idpazienti);//recuperto tutti i dati dalla tabella pazienti in base agli id ricavati sopra
		}
	}
	else{
		$pazienti=$this->_direttoreModel->getpazienti(); //se non ho il filtro prendo tutti i pazienti
	}	
	
	
	$seguiti=array(''=>'');
	$nome= array();
	$cognome= array();
	$sesso=array();
	$eta=array();
	$n=0;
	foreach($pazienti as $p){ 
		$seguiti+= array($n++=>$p->idpaziente);	//filtro le righe della tabella segui
		$nome += array($p->idpaziente => $p->nome);			//salvo le informazioni del paziente in array associativi per poi inviarli alla vista
		$cognome += array($p->idpaziente => $p->cognome);	
		$sesso += array($p->idpaziente => $p->genere);	
		$eta += array($p->idpaziente => $p->eta);	
	}
	
	$clini=array(''=>'');
	$i=0;
	$segue=$this->_direttoreModel->recuperasegue($seguiti);	//recupero le righe della tabella segue in base agli id dei pazienti
	foreach($segue as $x) $clini+=array($i++=>$x->idclini); //recupera gli id dei clinici di interesse
	
	$clini=$this->_direttoreModel->recuperaclinici($clini);

	$this->view->assign(array(
	   				'pnome' 	=>$nome,
	   				'pcognome'	=> $cognome,
	   				'psesso' 	=>$sesso,
	   				'peta' 		=>$eta,
	   				'clini'		=> $clini,
	   				'segue'		=> $segue	   				
					));
	}
	public function filtra(){
		
	}
	
	private function getfiltraForm(){
		$urlHelper = $this->_helper->getHelper('url');
		$this->_filtraform = new Application_Form_Direttore_Filtra();
		$this->_filtraform->setAction($urlHelper->url(array(
				'controller' => 'direttore',
				'action' => 'cliniciepazienti',
				'filtra' => TRUE),
				'default', TRUE
				));
		return $this->_filtraform;
	}



/**************************disturbi*********/
	public function newdisturboAction(){
		
	}
	
	public function disturboAction(){
		$test=$this->_direttoreModel->getDisturbo();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	
	public function adddisturboAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('disturbo');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newdisturbo');
		}
		$values = $form->getValues();
		$this->_direttoreModel->saveDisturbo($values);
		$this->_helper->redirector('disturbo'); /* rimanda alla action di admin controller*/
 	}	   
		
		private function getDisturboForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Direttore_Disturbo_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'addDisturbo'),
					'default',
					true
					));
			return $this->_form;
	}   
		public function cancelladisturboAction(){			
		$riga = $this->_getParam('disturbo');
		$this->_direttoreModel->cancellaDisturbo($riga);
		$this->_helper->redirector('disturbo');
	}
		
	public function modificadisturboAction()
	{
		
	}
	
	private function getUpdatedisturboForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formdisturbo = new Application_Form_Direttore_Disturbo_Moddisturbo;
	    $this->_formdisturbo->setAction($urlHelper->url(array(
                'controller' => 'direttore',
                'action' => 'updatedisturbo',),
                'default'
        ));
       return $this->_formdisturbo;
	}
	
	public function updatedisturboAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('disturbo');
        }
        $form=$this->_formdisturbo;
        if (!$form->isValid($_POST)) {
            return $this->render('modificadisturbo');
        }
		$var= $this->_getParam('disturbo');
        //$id = $this->_clinicoModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                                            );
        $this->_direttoreModel->updateDisturbo($data, $var);
        $this->_helper->redirector('disturbo');
	}
	
/******************************************clinico************************/
public function clinicoAction(){
		$test=$this->_direttoreModel->getClinico();
		$this->view->assign(array(
 		   				'riga' => $test,
    				));
	}
public function viewclinicoAction()
{
	$idc=$this->_getparam('id');

	if(!is_null($idc)){
		$test=$this->_direttoreModel->getClinicoById($idc);
	}else{
		$this->_helper->redirector('clinico');
	}
	$this->view->assign(array(
		   			'clinico' => $test,
		   			'from'	=>$this->_getparam('from')
			));
}
	
	
/************************************************logout*******************/

		public function logoutAction()
	{
		$this->_authService->clear();
		return $this->_helper->redirector('index','public');	
	}
/**************************************farmaci************************/
	public function farmacoAction(){
		$test=$this->_direttoreModel->getFarmaci();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
	public function newfarmacoAction(){
		
	}
	public function addfarmacoAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('farmaco');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newfarmaco');
		}
		$values = $form->getValues();
		$this->_direttoreModel->saveFarmaco($values);
		$this->_helper->redirector('farmaco'); /* rimanda alla action di admin controller*/
 	}	   
		
		private function getFarmacoForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Direttore_Farmaco_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'addfarmaco'),
					'default',
					true
					));
			return $this->_form;
	}   
		public function cancellafarmacoAction(){			
		$idfarmaco = $this->_getParam('farmaco');
		$this->_direttoreModel->cancellaFarmaco($idfarmaco);
		$this->_helper->redirector('farmaco');
	}
		
	public function modificafarmacoAction(){}
	
	private function getUpdatefarmacoForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formfarmaco = new Application_Form_Direttore_Farmaco_Modfarmaco;
	    $this->_formfarmaco->setAction($urlHelper->url(array(
                'controller' => 'direttore',
                'action' => 'updatefarmaco',),
                'default'
        ));
       return $this->_formfarmaco;
	}
	
	public function updatefarmacoAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('farmaco');
        }
        $form=$this->_formfarmaco;
        if (!$form->isValid($_POST)) {
            return $this->render('modificafarmaco');
        }
		$var= $this->_getParam('farmaco');
        //$id = $this->_clinicoModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                                            );
        $this->_direttoreModel->updateFarmaco($data, $var);
        $this->_helper->redirector('farmaco');
	}

/**************************************attività************************/
public function attivitaAction(){
		$test=$this->_direttoreModel->getAttivita();
		$this->view->assign(array(
			   		'riga' => $test,
    				));
		}
public function newattivitaAction(){
		
	}
public function addattivitaAction()
	{
		if (!$this->getRequest()->isPost()) {
				$this->_helper->redirector('attivita');
			}
		$form=$this->_form;
		if (!$form->isValid($_POST)) {
			return $this->render('newattivita');
		}
		$values = $form->getValues();
		$this->_direttoreModel->saveAttivita($values);
		$this->_helper->redirector('attivita'); /* rimanda alla action di admin controller*/
 	}	   
		
		private function getAttivitaForm () {	
			$urlHelper = $this->_helper->getHelper('url');
			$this->_form = new Application_Form_Direttore_Attivita_Add();
			$this->_form->setAction($urlHelper->url(array(
					'controller' => 'direttore',
					'action' => 'addattivita'),
					'default',
					true
					));
			return $this->_form;
	}   
		public function cancellaattivitaAction(){			
		$idattivita = $this->_getParam('attivita');
		$this->_direttoreModel->cancellaAttivita($idattivita);
		$this->_helper->redirector('attivita');
	}
		
	public function modificaattivitaAction()
	{
		
	}
	
	private function getUpdateattivitaForm(){
		$urlHelper = $this->_helper->getHelper('url');
        $this->_formattivita = new Application_Form_Direttore_Attivita_Modattivita;
	    $this->_formattivita->setAction($urlHelper->url(array(
                'controller' => 'direttore',
                'action' => 'updateattivita',),
                'default'
        ));
       return $this->_formattivita;
	}
	
	public function updateattivitaAction(){
		if(!$this->getRequest()->isPost()) {
            $this->_helper->redirector('attivita');
        }
        $form=$this->_formfarmaco;
        if (!$form->isValid($_POST)) {
            return $this->render('modificaattivita');
        }
		$var= $this->_getParam('attivita');
        //$id = $this->_clinicoModel->getClinicoById($var);//a che serve?
        $values = $form->getValues();
        $data = array(    
                        'nome' =>$values['nome'],
                                            );
        $this->_direttoreModel->updateAttivita($data, $var);
        $this->_helper->redirector('attivita');
	}
}
