function ohSnap(text, color) {
    var html,
        time = '10000',
        $container = $('#ohsnap');

    // Generate the HTML
    html = $('<div class="alert alert-' + color + '">' + text + '</div>').fadeIn('fast');

    // Append the label to the container
    $container.append(html);

    // Remove the notification on click
    html.on('click', function() {
        ohSnapX($(this));
    });

    // After 'time' seconds, the animation fades out
    setTimeout(function() {
        ohSnapX($container.children('.alert').first());
    }, time);
}

function ohSnapX(element) {
    // Called without argument, the function removes all alerts
    // element must be a jQuery object

    if (typeof element !== "undefined") {
        element.fadeOut('fast', function() {
            $(this).remove();
        });
    } else {
        $('.alert').fadeOut('fast', function() {
            $(this).remove();
        });
    }
}



function notifica(cambio){
	
		if(cambio > 0){
		$("#profile").addClass('notify');
		$("#profile").removeClass('current_page_item');
		ohSnap('Controlla i cambiamenti nella tua terapia!', 'red');
		}else if(cambio === 0){
			$("#profile").hide();
		}else
			$("#profile").hide();
	}
	
function correct(){
	ohSnap('Episodio inserito correttamente', 'green');
}
